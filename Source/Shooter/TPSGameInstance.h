// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Types.h"
#include "Engine/DataTable.h"
#include "WeaponDefault.h"
#include "TPSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API UTPSGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponSettings")
		UDataTable* WeaponInfoTaple = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponSettings")
		UDataTable* DropItemInfoTaple = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponSettings")
		UDataTable* EquipmentInfoTaple = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "WeaponSettings")
		TMap<FString, FInventory> InventoryForPlayers;

protected:

public:

	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetEquipmentInfoByName(FName EquipName, FEquipmentInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
		void AddMenuWeaponToInventory(FName WeaponName, int32 Round, FString PlayerName);
	UFUNCTION(BlueprintCallable)
		void RemoveMenuWeaponFromInventory(FName WeaponName, FString PlayerName);
	UFUNCTION(BlueprintCallable)
		void UpdateInventoryForPlayer(FString PlayerName, int32 SumWeaponSlots, int32 SumAmmoSlots);
	UFUNCTION(BlueprintCallable)
		FInventory GetInventoryForPlayer(FString PlayerName);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void AddNewInventoryForPlayer_OnServer(const FInventory &NewInventory, const FString &PlayerName);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void AddNewInventoryForPlayer_Multicast(const FInventory &NewInventory, const FString &PlayerName);

};
