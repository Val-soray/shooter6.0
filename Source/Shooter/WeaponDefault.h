// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Types.h"
#include "WeaponDefault.generated.h"
//#include "Weapons/Progectiles/ProjectileDefault.h"

using namespace UF;
using namespace UP;

class ASleeveBullets;
class AMagazin;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFire);

UCLASS()
class SHOOTER_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* SleeveLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* MagazinLocation = nullptr;

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponFire OnWeaponFire;

	FWeaponInfo WeaponSettings;
	UPROPERTY(Replicated, BlueprintReadWrite)
	FAdditionalWeaponInfo AdditionalWeaponInfo;
	UPROPERTY(Replicated, BlueprintReadOnly)
	FVector ShootEndLocation = FVector(0);

	float FireTimer = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0;
	//Debug
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
	float ReloadTime = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bIsFire = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool bIsWeaponReloading = false;

	//Dispersion
	UPROPERTY(Replicated)
	bool bShouldReduceDispersion = false;
	UPROPERTY(BlueprintReadWrite)
	float CurrentDispersion = 0;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	bool bCanWeaponFire = true;

	bool bIsWeaponFiring = false;

	float TimeToEjection = WeaponSettings.TimeToMagazinEjection;

	UPROPERTY(EditAnywhere)
		TSubclassOf<ASleeveBullets> SleeveBulletsClass;

	UPROPERTY(EditAnywhere)
		TSubclassOf<AMagazin> MagazinClass;

	UPROPERTY(BlueprintReadOnly)
		bool bIsAmmoEmpty = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void WeaponInit(FWeaponInfo Info);

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);

	void SpawnBullet();

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();
	UFUNCTION(Server, Reliable)
	void InitReload_OnServer();
	void FinishReload();



	void ChangeDispersion();

	UFUNCTION(BlueprintCallable)
		FVector GetFireEndLocation() const;

	UFUNCTION()
		FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	UFUNCTION()
		float GetCurrentDispersion() const;



	UFUNCTION()
		void DispersionTick(float DeltaTime);

	UFUNCTION()
		void ChangeDispersionByShot();

	UFUNCTION()
		int8 GetNumberProjectileByShot() const;

	UFUNCTION()
		void CancelReload();

	UFUNCTION()
		bool CanWeaponReload();

	int32 GetAvailableAmmoForReload();

	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion, FVector Displacement);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFiring, bool bIsResetFireTimer);	
		
	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);

	UFUNCTION(NetMulticast, Reliable)
	void WeaponFireAnim_Muticast(UAnimMontage* FireAnim);	
	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform DropOffset, FVector DropImpulseDirection, float LifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);
	UFUNCTION(NetMulticast, Reliable)
	void InitDropMesh_Multicast(UStaticMesh* DropMesh, FTransform DropOffset, FVector DropImpulseDirection, float LifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDirection);

	UFUNCTION(NetMulticast, Reliable)
	void FXWeaponFire_Multicast(UParticleSystem* FireFX, USoundBase* FireSound);

	UFUNCTION(NetMulticast, Reliable)
	void HideBoneByName_Multicast(FName BoneToHide);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnTraceHitDecal_Multicast(UMaterialInterface* Material, FVector Location, FRotator Rotation, float Size);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnTraceHitFX_Multicast(UParticleSystem* FX, FRotator Rotation, FVector Location, FVector Size);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnTraceHitSound_Multicast(USoundBase* Sound, FVector Location);

};
