// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_StateEffect.h"
#include "TPSHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TPS_IGameActor.h"
#include "TPSCharacter.h"
#include "TPSCharacterHealthComponent.h"

//Parent Effect
bool UTPS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;

	ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(myActor);
	if (Interface)
	{
		Interface->AddEffect(this);
	}

	if (ParticleEffect)
	{
		FName NameBoneToAttach = NameBoneHit;
		FVector Location;
		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttach, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttach, Location, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
	if (!bIsEndLess)
	{
		if(Timer > 0)
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_Effect, this, &UTPS_StateEffect::DestroyObject, Timer, false);
		}
		else
		{
			Execute();
			DestroyObject();
		}
	}

	return true;
}

void UTPS_StateEffect::DestroyObject()
{	
	ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(myActor);
	if (Interface)
	{
		Interface->RemoveEffect(this);
	}

	myActor = nullptr;

	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
	}
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

void UTPS_StateEffect::Execute()
{
}




//Execute Once
void UTPS_StateEffect_ExecuteOnce::Execute()
{
	
}

bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	Execute();

	return true;
}

void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}




//Execute Timer
void UTPS_StateEffect_ExecuteTimer::Execute()
{
}

bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_Execute, this, &UTPS_StateEffect_ExecuteTimer::Execute, RateTime, true);
	}
	
	return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	Super::DestroyObject();
}




//Execute Once HealthEffect
void UTPS_StateEffect_ExecuteOnce_HealthEffect::Execute()
{	
	Super::Execute();
	if (myActor)
	{
		UTPSHealthComponent* HealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (HealthComp)
		{
			HealthComp->ChangeCurrentHealth(Power);
		}
	}

}




//Execute Timer HealthEffect
void UTPS_StateEffect_ExecuteTimer_HealthEffect::Execute()
{
	Super::Execute();
	if (myActor)
	{
		UTPSHealthComponent* HealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (HealthComp)
		{
			HealthComp->ChangeCurrentHealth(Power);
		}
	}
}




//Execute Once StunEffect
void UTPS_StateEffect_ExecuteOnce_StunEffect::Execute()
{
	Super::Execute();

	if (myActor)
	{
		ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(myActor);
		if (Interface)
		{
			Interface->Stun();
		}
	}
}

void UTPS_StateEffect_ExecuteOnce_StunEffect::DestroyObject()
{
	if (myActor)
	{
		ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(myActor);
		if (Interface)
		{
			Interface->EndStun();
		}
	}
	Super::DestroyObject(); 
}




//Execute Once InvulnerableEffect
void UTPS_StateEffect_ExecuteOnce_InvulnerableEffect::Execute()
{
	if (myActor)
	{
		ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(myActor);
		if (Interface)
		{
			Interface->Invulnerable();
		}
	}
}

void UTPS_StateEffect_ExecuteOnce_InvulnerableEffect::DestroyObject()
{
	if (myActor)
	{
		ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(myActor);
		if (Interface)
		{
			Interface->EndInvulnerable();
		}
	}
	Super::DestroyObject();
}




//Execute Ocne HealthBoost
void UTPS_StateEffect_ExecuteOnce_HealthBoostEffect::Execute()
{
	if (myActor)
	{
		if(myActor->GetComponentByClass(UTPSCharacterHealthComponent::StaticClass()))
		{
			UTPSCharacterHealthComponent* HealthComp = Cast<UTPSCharacterHealthComponent>(myActor->GetComponentByClass(UTPSCharacterHealthComponent::StaticClass()));
			if (HealthComp)
			{
				HealthComp->SetHealthBoost(Power);
			}
		}

	}
}
