// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSInventoryComponent.h"
#include "TPSGameInstance.h"
#include "TPS_IGameActor.h"
#include "TPSCharacter.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UTPSInventoryComponent::UTPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UTPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void UTPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	int32 CorrectIndex = OldIndex;
	int32 Direction = ChangeToIndex - OldIndex;
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	bool bIsSuccess = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		CorrectIndex += Direction;

		if (WeaponSlots.IsValidIndex(CorrectIndex))
		{
			if(!WeaponSlots[CorrectIndex].NameItem.IsNone())
			{
				if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
				{
					NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
					NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
					bIsSuccess = true;
				}
				else
				{
					FWeaponInfo CurrentInfo;
					UTPSGameInstance* GI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
					
					if (GI)
					{
						if(GI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, CurrentInfo))
						{
							int8 j = 0;
							bool bIsFind = false;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == CurrentInfo.WeaponType)
								{
									if (AmmoSlots[j].Cout > 0)
									{
										NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
										NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
										bIsSuccess = true;
									}
								}
								j++;
							}
						}
					}
				}
			}
			
		}
		else
		{
			if (CorrectIndex > WeaponSlots.Num() - 1)
			{
				CorrectIndex = -1;
			}
			else if (CorrectIndex < 0)
			{
				CorrectIndex = WeaponSlots.Num();
			}
		}
		i++;
	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, CorrectIndex);

		ATPSCharacter* Char = Cast<ATPSCharacter>(GetOwner());
		if(Char && Char->GetMesh() && Char->GetMesh()->GetAnimInstance())
		{
			if (Char->SumSprint != 0)
			{
				Char->bIsSprint = false;
				Char->SumSprint = 0;
				Char->ChangeMovementState(EMovementState::Run_State);
			}
			if(Char->CurrentWeapon)
			{
				Char->CurrentWeapon->bCanWeaponFire = false;
			}
			float SwitchWeaponAnimTime = Char->GetMesh()->GetAnimInstance()->Montage_Play(Char->SwitchWeaponAnim);
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_Switch, this, &UTPSInventoryComponent::EndSwitchWeapon, SwitchWeaponAnimTime, false);
		}
		
	}

	return bIsSuccess;
}

FAdditionalWeaponInfo UTPSInventoryComponent::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	return FAdditionalWeaponInfo();
}

int32 UTPSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 Result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			Result = i;
		}
		else
		{
			i++;
		}
	}

	return Result;
}

FName UTPSInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName Result;
	if(WeaponSlots.IsValidIndex(IndexSlot))
	{
		Result = WeaponSlots[IndexSlot].NameItem;	
	}

	return Result;
}


void UTPSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				ChangeWeaponAdditionalInfo_Multicast(IndexWeapon, NewInfo);
				//OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (bIsFind == false)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No found weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Not correct index weapon - %d"), IndexWeapon);
	}
}

void UTPSInventoryComponent::WeaponChangeAmmo(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bWasEmpty = false;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			if (AmmoSlots[i].Cout <= 0)
			{
				bWasEmpty = true;
			}
			AmmoSlots[i].Cout += CoutChangeAmmo;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
			{
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}
			ChangeAmmoEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			//OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			if (bWasEmpty && AmmoSlots[i].Cout > 0)
			{
				AmmoTypeEmpty_Multicast(TypeWeapon, false);
				//OnAmmoTypeEmpty.Broadcast(TypeWeapon, false);
			}
			if (AmmoSlots[i].Cout <= 0)
			{
				AmmoTypeEmpty_Multicast(TypeWeapon, true);
				//OnAmmoTypeEmpty.Broadcast(TypeWeapon, true);
			}
			bIsFind = true;
		}
		i++;
	}
}

bool UTPSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AvaibleAmmoForWeapon)
{	
	bool Result = false;
	AvaibleAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AvaibleAmmoForWeapon = AmmoSlots[i].Cout;
			bIsFind = true;
			if(AmmoSlots[i].Cout > 0)
			{
				Result = true;
			}
			else
			{
				Result = false;
			}
		}
		i++;
	}
	return Result;
}

bool UTPSInventoryComponent::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool Result = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !Result)
	{
		if (AmmoSlots[i].WeaponType == AmmoType)
		{
			Result = AmmoSlots[i].Cout < AmmoSlots[i].MaxCout;
		}
		i++;
	}
	return Result;
}

bool UTPSInventoryComponent::CheckCanTakeWeapon(int32 &FreeSlotNumber)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFind = true;
			FreeSlotNumber = i;
		}
		i++;
	}
	return bIsFind;
}

bool UTPSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeapon, FDropItem& DropItemInfo)
{
	bool Result = false;
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;
		if(IndexSlot == CurrentIndexWeapon)
		{
			SwitchWeaponToIndex(CurrentIndexWeapon, CurrentIndexWeapon, NewWeapon.AdditionalInfo);
		}
		UpdateWeaponSlots_Multicast(IndexSlot, NewWeapon);
		//OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		Result = true;
	}
	return Result;
}

void UTPSInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickUpActor, FWeaponSlot NewWeapon)
{
	int32 FreeSlotNumber;
	if (CheckCanTakeWeapon(FreeSlotNumber))
	{
		if (WeaponSlots.IsValidIndex(FreeSlotNumber))
		{
			WeaponSlots[FreeSlotNumber] = NewWeapon;
			UpdateWeaponSlots_Multicast(FreeSlotNumber, NewWeapon);
			
			if (PickUpActor)
			{
				PickUpActor->Destroy();
			}
		}
	}
}

bool UTPSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool Result = false;
	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);
	UTPSGameInstance* GI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (GI)
	{
		Result = GI->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{	
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}
	return Result;
}

void UTPSInventoryComponent::DropWeaponByIndex(int32 ByIndex, FDropItem& DropItemInfo)
{
	FWeaponSlot EmptyWeaponSlot;

	if(SwitchWeaponToIndex(ByIndex + 1, ByIndex, WeaponSlots[ByIndex].AdditionalInfo))
	{
		GetDropItemInfoFromInventory(ByIndex, DropItemInfo);

		for (int8 i = ByIndex; i < WeaponSlots.Num(); i++)
		{
			if (WeaponSlots.IsValidIndex(i + 1))
			{
				WeaponSlots[i] = WeaponSlots[i + 1];
				UpdateWeaponSlots_Multicast(i, WeaponSlots[i]);
				//OnUpdateWeaponSlots.Broadcast(i, WeaponSlots[i]);
			}
			else
			{
				WeaponSlots[i] = EmptyWeaponSlot;
				UpdateWeaponSlots_Multicast(i, EmptyWeaponSlot);
				//OnUpdateWeaponSlots.Broadcast(i, EmptyWeaponSlot);
			}
		}

		if (GetOwner()->GetClass()->ImplementsInterface(UTPS_IGameActor::StaticClass()))
		{
			ITPS_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}

	}
}

void UTPSInventoryComponent::EndSwitchWeapon()
{
	ATPSCharacter* Char = Cast<ATPSCharacter>(GetOwner());

	if (Char && Char->GetMesh() && Char->GetMesh()->GetAnimInstance() && Char->CurrentWeapon)
	{
		Char->CurrentWeapon->bCanWeaponFire = true;
		Char->bIsSprint = true;
	}
}

void UTPSInventoryComponent::UpdateWeaponSlots_Multicast_Implementation(int32 WeaponSlotNumber, FWeaponSlot WeaponSlotToUpdate)
{
	OnUpdateWeaponSlots.Broadcast(WeaponSlotNumber, WeaponSlotToUpdate);
}

void UTPSInventoryComponent::AmmoTypeEmpty_Multicast_Implementation(EWeaponType WeaponType, bool bIsEmpty)
{
	OnAmmoTypeEmpty.Broadcast(WeaponType, bIsEmpty);
}

void UTPSInventoryComponent::WeaponAmmoEmpty_Multicast_Implementation(int32 IndexSlot, bool bIsEmpty)
{
	OnWeaponAmmoEmpty.Broadcast(IndexSlot, bIsEmpty);
}

void UTPSInventoryComponent::ChangeWeaponAdditionalInfo_Multicast_Implementation(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void UTPSInventoryComponent::ChangeAmmoEvent_Multicast_Implementation(EWeaponType WeaponType, int32 Cout)
{
	OnAmmoChange.Broadcast(WeaponType, Cout);
}

void UTPSInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot> &NewWeaponSlotsInfo, const TArray<FAmmoSlot> &NewAmmoSlotsInfo, const TArray<FEquipmentSlot> &NewEquipmentSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;

	TArray<FEquipmentSlot> OldEquipSlots = EquipSlots;
	EquipSlots = NewEquipmentSlotsInfo;

	if (WeaponSlots.IsValidIndex(0) && !WeaponSlots[0].NameItem.IsNone())
	{
		SwitchWeaponToIndex(0, 0, WeaponSlots[0].AdditionalInfo);
	}
	for (int8 i = 0; i < EquipSlots.Num(); i++)
	{
		FName OldName = NAME_None;
		if (OldEquipSlots.IsValidIndex(i) && !OldEquipSlots[i].ItemName.IsNone())
		{
			OldName = OldEquipSlots[i].ItemName;
		}
		OnPutOnEquipment.Broadcast(EquipSlots[i].ItemName, OldName);
	}
}

void UTPSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UTPSInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UTPSInventoryComponent, AmmoSlots);
	DOREPLIFETIME(UTPSInventoryComponent, EquipSlots);
}