// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Animation/SkeletalMeshActor.h"
#include "Types.generated.h"

using namespace UF;

class AProjectileDefault;
class AWeaponDefault;
class UTPS_StateEffect;

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	Sprint_State UMETA(DisplayName = "Sprint State"),
	WalkAim_State UMETA(DisplayName = "WalkAim State")
};


UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotgunType UMETA(DisplayName = "Shotgun"),
	SniperRifle UMETA(DisplayName = "Sniper Rifle"),
	GrenadeLauncher UMETA(DisplayName = "Grenade Launcher"),
	RocketLauncher UMETA(DisplayName = "Rocket Launcher")
};

UENUM(BlueprintType)
enum class EEquipmentType : uint8
{
	Head UMETA(DisplayName = "Head"),
	Body UMETA(DisplayName = "Body"),
	Legs UMETA(DisplayName = "Legs"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeed = 350.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 250.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeed = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintSpeed = 750.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkAimSpeed = 150.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TSubclassOf<class AProjectileDefault>Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileInitSpeed = 2000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileMaxSpeed = 2000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		bool bIsLikeBomb = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileMaxDamageRadius = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ExploseMaxDamage = 100;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ExploseRadius = 300;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float TimeToExplose = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		FName ProjectileName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSounds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		UParticleSystem* ExploseFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		USoundBase* ExploseSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileMesh")
		UStaticMesh* BulletsMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BulletFX")
		UParticleSystem* BulletFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
		TSubclassOf<UTPS_StateEffect> Effect = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Aim_StateDispersionReduction = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		int32 MaxRound = 25;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		int32 NumberProjectileByShot = 1;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		float TimeToMagazinEjection = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sleeve")
		float SleeveLifeTime = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		float MagazinLifeTime = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		FName WeaponName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		float AttachMagazinToHandTime = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		float EndReloadTime = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sleeve")
		float SleeveDropImpulsePower = 10.0;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sleeve")
		float SleeveDropImpulseDispersion = 15.0;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sleeve")
		float SleeveCustomMass = 0.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		float MagazinDropImpulsePower = 10.0;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		float MagazinDropImpulseDispersion = 15.0;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		float MagazinCustomMass = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjetileSettings")
		FProjectileInfo ProjectileSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		FWeaponDispersion WeaponDispersion;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sounds")
		USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sounds")
		USoundBase* SoundReloadWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* FireWeaponEffect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float DistanceTrace = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterAnimFire")
		UAnimMontage* AnimCharFireIronsights = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterAnimFire")
		UAnimMontage* AnimCharFireHip = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterAnimReload")
		UAnimMontage* AnimCharReloadHip = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterAnimReload")
		UAnimMontage* AnimCharReloadIronsights = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponAnimFire")
		UAnimMontage* AnimWeaponFire = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magazin")
		UStaticMesh* MagazinDropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sleeve")
		UStaticMesh* SleeveBulletsMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		USkeletalMesh* WeaponSkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		float TimeToSwitchWeapon = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		EWeaponType WeaponType = EWeaponType::RifleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bone")
		FName BoneName = FName("");

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
	bool bShouldUseDisplacement = true;
};

USTRUCT(BlueprintType)
struct FEquipmentInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		USkeletalMesh* SkeletalMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		EEquipmentType DefenceArea;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		float Protection = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		float SpeedBoost = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		TArray<FName> SocketNames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		UTexture2D* EquipmentIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		FName ItemName = NAME_None;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentParams")
		TArray<FName> BonesToHide;

};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStats")
		int32 Round = 5;
};


USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FName NameItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		EWeaponType WeaponType = EWeaponType::RifleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 Cout = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 MaxCout = 100;
};

USTRUCT(BlueprintType)
struct FEquipmentSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentSlot")
		EEquipmentType EquipmentType = EEquipmentType::Head;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EquipmentSlot")
		FName ItemName = NAME_None;

};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		USkeletalMesh * WeaponSkeletalMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		FVector MeshSpawnLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropItem")
		FWeaponSlot WeaponInfo;

};

USTRUCT(BlueprintType)
struct FInventory : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TArray<FAmmoSlot> AmmoSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TArray<FEquipmentSlot> EquipmentSlots;
};

USTRUCT(BlueprintType)
struct FEquipmentByType
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Equipment")
	EEquipmentType EquipType = EEquipmentType::Head;
	UPROPERTY(BlueprintReadWrite, Category = "Equipment")
	TArray<ASkeletalMeshActor*> SkeletalMesh;
};

UCLASS()
class  UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	static UTPS_StateEffect* AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType, FName NameBoneHit);
};
