// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Types.h"
#include "TPSInventoryComponent.generated.h"

using namespace UF;
using namespace UP;
//Delegates
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewWeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAmmoEmpty, int32, WeaponIndex, bool, bIsEmpty);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, WeaponSlotNumber, FWeaponSlot, WeaponSlotToUpdate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoTypeEmpty, EWeaponType, WeaponType, bool, bIsEmpty);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPutOnEquipment ,FName, NewEquipmentName, FName, OldEquipmentName);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER_API UTPSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTPSInventoryComponent();

	FOnPutOnEquipment OnPutOnEquipment;

	FOnSwitchWeapon OnSwitchWeapon;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoChange OnAmmoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnUpdateWeaponSlots OnUpdateWeaponSlots;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	FOnAmmoTypeEmpty OnAmmoTypeEmpty;
	//Timer
	FTimerHandle TimerHandle_Switch;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TArray<FAmmoSlot> AmmoSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Equipment")
		TArray<FEquipmentSlot> EquipSlots;

	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo);
	FAdditionalWeaponInfo GetAdditionalWeaponInfo(int32 IndexWeapon);
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	FName GetWeaponNameBySlotIndex(int32 IndexSlot);
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);
	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int32& AvaibleAmmoForWeapon);
	UFUNCTION(BlueprintCallable)
	void WeaponChangeAmmo(EWeaponType TypeWeapon, int32 CoutChangeAmmo);

	//Interface PickUp Ators
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeAmmo(EWeaponType AmmoType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeWeapon(int32 &FreeSlotNumber);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeapon, FDropItem &DropItemInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void TryGetWeaponToInventory_OnServer(AActor* PickUpActor, FWeaponSlot NewWeapon);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		void DropWeaponByIndex(int32 ByIndex, FDropItem& DropItemInfo);

	UFUNCTION()
	void EndSwitchWeapon(); 

	UFUNCTION(Server, Reliable, BlueprintCallable)
		void InitInventory_OnServer(const TArray<FWeaponSlot> &NewWeaponSlotsInfo, const TArray<FAmmoSlot> &NewAmmoSlotsInfo, const TArray<FEquipmentSlot> &NewEquipmentSlotsInfo);

	UFUNCTION(NetMulticast, Reliable)
	void ChangeAmmoEvent_Multicast(EWeaponType WeaponType, int32 Cout);
	UFUNCTION(NetMulticast, Reliable)
	void ChangeWeaponAdditionalInfo_Multicast(int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponAmmoEmpty_Multicast(int32 IndexSlot, bool bIsEmpty);
	UFUNCTION(NetMulticast, Reliable)
	void AmmoTypeEmpty_Multicast(EWeaponType WeaponType, bool bIsEmpty);
	UFUNCTION(NetMulticast, Reliable)
	void UpdateWeaponSlots_Multicast(int32 WeaponSlotNumber, FWeaponSlot WeaponSlotToUpdate);

};
