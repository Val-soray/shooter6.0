// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterEnemy_Base.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Materials/MaterialInterface.h"
#include "TPSHealthComponent.h"
#include "Types.h"

// Sets default values
ACharacterEnemy_Base::ACharacterEnemy_Base()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = CreateDefaultSubobject<UTPSHealthComponent>(TEXT("Health"));
}

// Called when the game starts or when spawned
void ACharacterEnemy_Base::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACharacterEnemy_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterEnemy_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

EPhysicalSurface ACharacterEnemy_Base::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (GetMesh())
	{
		UMaterialInterface* Material = GetMesh()->GetMaterial(0);
		if (Material)
		{
			Result = Material->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTPS_StateEffect*> ACharacterEnemy_Base::GetAllCurrentEffects()
{
	return Effects;
}

void ACharacterEnemy_Base::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ACharacterEnemy_Base::AddEffect(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}
