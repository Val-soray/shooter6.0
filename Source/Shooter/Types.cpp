// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "Shooter.h"
#include "TPS_StateEffect.h"
#include "TPS_IGameActor.h"

UTPS_StateEffect* UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType, FName NameBoneHit)
{
	UTPS_StateEffect* ReturnEffect = nullptr;
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && AddEffectClass && TakeEffectActor)
	{
		UTPS_StateEffect* Effect = Cast<UTPS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (Effect)
		{
			bool bIsPossibleSurface = false;
			int8 i = 0;
			while (!bIsPossibleSurface && i < Effect->PossibleInteractSurface.Num())
			{
				if (Effect->PossibleInteractSurface[i] == SurfaceType)
				{	
					bIsPossibleSurface = true;
					bool bIsCanAddEffect = true;
					if (!Effect->bIsStakable)
					{
						TArray<UTPS_StateEffect*> CurrentEffects;
						ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(TakeEffectActor);
						if(Interface)
						{	
							CurrentEffects = Interface->GetAllCurrentEffects();
						}	
						int8 j = 0;
						while (bIsCanAddEffect && j < CurrentEffects.Num())
						{
							if (CurrentEffects[j]->GetClass() == AddEffectClass)
							{
								bIsCanAddEffect = false;
								
							}
							j++;
						}
					}
					if(bIsCanAddEffect)
					{
						UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
							ReturnEffect = NewEffect;
						}
					}

				}
				i++;
			}
		}
		
	}
	return ReturnEffect;
}
