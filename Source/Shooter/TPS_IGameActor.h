// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Types.h"

#include "TPS_IGameActor.generated.h"

class UTPS_StateEffect;

using namespace UF;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */

class SHOOTER_API ITPS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//Effects
	bool bIsStuned = false;
	bool bIsInvulnerable = false;

	virtual EPhysicalSurface GetSurfaceType();
	//Effects
		virtual TArray<UTPS_StateEffect*> GetAllCurrentEffects();
		virtual void RemoveEffect(UTPS_StateEffect* RemoveEffect);
		virtual void AddEffect(UTPS_StateEffect* NewEffect);
		//Stun
			virtual void Stun();
			virtual void EndStun();
		//Invulnerable
			virtual void Invulnerable();
			virtual void EndInvulnerable();
			virtual bool IsInvulnerable();
	//Inventory
		UFUNCTION(BlueprintImplementableEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
		UFUNCTION(BlueprintImplementableEvent)
		void DropAmmoToWorld(FDropItem DropItemInfo);
};
