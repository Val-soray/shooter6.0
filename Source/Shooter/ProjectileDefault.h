// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Types.h"
#include "ProjectileDefault.generated.h"

using namespace UF;
using namespace UP;

class AWeaponDefault;

UCLASS()
class SHOOTER_API AProjectileDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent* BulletSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
	FProjectileInfo ProjectileInfo;

	FVector LastProjectileLocation;

	AWeaponDefault* Owner;

	float ProjectileDamage = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION(BlueprintCallable)
		virtual void InitProjectile(FProjectileInfo InitParam);
	UFUNCTION(Server, Reliable)
	virtual void HitResult_OnServer(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit); 
	UFUNCTION()
	void BeginOverlapResult(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void EndOverlapResult(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual void ImpactProjectile();

	UFUNCTION()
		void ChangeProjectileDamage();

	//Init Projectile (Multicast)
	UFUNCTION(NetMulticast, Reliable)
	void InitVisualProjectileMesh_Multicast(UStaticMesh* NewMesh);
	UFUNCTION(NetMulticast, Reliable)
	void InitVisualProjectileTrail_Multicast(UParticleSystem* NewTrail);

	// Hit Effects
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitFX_Multicast(UParticleSystem* FXTemplate, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitSound_Multicast(USoundBase* SoundBase, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
	void SetProjectileMovementSpeed_Multicast(float NewInitSpeed, float NewMaxSpeed);
};
