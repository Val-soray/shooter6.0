// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "TPS_StateEffect.generated.h"

using namespace UF;
using namespace UP;

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect : public UObject
{
	GENERATED_BODY()
	
public:

	AActor* myActor;

	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	UFUNCTION(BlueprintCallable)
	virtual void DestroyObject();
	virtual void Execute();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bIsStakable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		UParticleSystem* ParticleEffect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bIsEndLess = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Timer")
		float Timer = 5.0f;

	UParticleSystemComponent* ParticleEmitter = nullptr;

	FTimerHandle TimerHandle_Effect;
};

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect_ExecuteOnce : public UTPS_StateEffect
{
	GENERATED_BODY()

public:

	void Execute() override;
	virtual bool InitObject(AActor* Actor, FName NameBoneHit) override;
	virtual void DestroyObject() override;
};

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect_ExecuteTimer : public UTPS_StateEffect
{
	GENERATED_BODY()

public:
	void Execute() override;
	virtual bool InitObject(AActor* Actor, FName NameBoneHit) override;
	virtual void DestroyObject() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Timer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_Execute;
};

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect_ExecuteOnce_HealthEffect : public UTPS_StateEffect_ExecuteOnce
{
	GENERATED_BODY()

public:

	void Execute() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Timer")
		float Power = 20.0f;
};

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect_ExecuteTimer_HealthEffect : public UTPS_StateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:

	void Execute() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute Once")
		float Power = 4.0f;
};

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect_ExecuteOnce_StunEffect : public UTPS_StateEffect_ExecuteOnce
{
	GENERATED_BODY()

public:

	void Execute() override;
	void DestroyObject() override;

};

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect_ExecuteOnce_InvulnerableEffect : public UTPS_StateEffect_ExecuteOnce
{
	GENERATED_BODY()

public:

	void Execute() override;
	void DestroyObject() override;
};

UCLASS(Blueprintable)
class SHOOTER_API UTPS_StateEffect_ExecuteOnce_HealthBoostEffect : public UTPS_StateEffect_ExecuteOnce
{
	GENERATED_BODY()

public:

	void Execute() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings Execute")
		float Power = 20.0f;

};

