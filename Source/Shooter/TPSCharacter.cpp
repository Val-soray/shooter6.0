
#include "TPSCharacter.h"
//#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Tank.h"
#include "Gate.h"
#include "WeaponDefault.h"
#include "TPSGameInstance.h"
//#include "Components/SkinnedMeshComponent.h"
#include "Magazin.h"
#include "Components/SceneComponent.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "TPSInventoryComponent.h"
#include "TPSPlayerController.h"
#include "Types.h"
#include "TPSCharacterHealthComponent.h"
#include "ProjectileDefault.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TPS_StateEffect.h"
#include "Animation/SkeletalMeshActor.h"
#include "Shooter.h"
#include "Net/UnrealNetwork.h"
#include "Engine/StaticMeshActor.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule


	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	CameraBoom->CameraLagSpeed = 5;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));

	Capsule->SetupAttachment(RootComponent);

	Capsule->InitCapsuleSize(42.f, 96.0f);

	Capsule->OnComponentBeginOverlap.AddDynamic(this, &ATPSCharacter::OverlapReaction);


	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("Inventory Component"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPSCharacter::InitWeapon);
		InventoryComponent->OnPutOnEquipment.AddDynamic(this, &ATPSCharacter::InitEquipment_OnServer);
	}	
	HealthComponent = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("Health Component"));
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ATPSCharacter::CharDead);
	}

	//NetWork

	bReplicates = true;
}


void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();
	/*SpawnTank();
	SpawnGate();*/
	SpawnCursor();

	ATPSPlayerController* PC = Cast<ATPSPlayerController>(GetController());
	if (PC)
	{
		PC->TPSCharacter = this;
	}
}

void ATPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MovementTick(DeltaTime);

	SprintTick(DeltaTime);

	MagazinReload_OnServer(DeltaTime);
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("Sprint"), this, &ATPSCharacter::InputSprint);
	NewInputComponent->BindAxis(TEXT("Walk"), this, &ATPSCharacter::InputWalk);
	NewInputComponent->BindAxis(TEXT("Aim"), this, &ATPSCharacter::InputAim);
	NewInputComponent->BindAxis(TEXT("Fire"), this, &ATPSCharacter::Fire);
	NewInputComponent->BindAxis(TEXT("SwitchWeapon"), this, &ATPSCharacter::SwitchWeapon_OnServer);
	NewInputComponent->BindAction(TEXT("Reload"),EInputEvent::IE_Pressed, this, &ATPSCharacter::ReloadWeapon_OnServer);
	NewInputComponent->BindAction(TEXT("FirstAidAbility"), EInputEvent::IE_Pressed, this, &ATPSCharacter::TryUseFirstAidAbility);
	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATPSCharacter::DropCurrentWeapon);


	TArray<FKey>HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATPSCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATPSCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATPSCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATPSCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATPSCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATPSCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATPSCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATPSCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATPSCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATPSCharacter::TKeyPressed<0>);
}

void ATPSCharacter::InputAxisX(float Value)
{
	ValueX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	ValueY = Value;
}

void ATPSCharacter::CharacterUpdate()
{
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	case EMovementState::WalkAim_State:
		ResSpeed = MovementInfo.WalkAimSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed + SpeedBoost* ResSpeed;
}

void ATPSCharacter::InputSprint(float Value)
{
	if (!bIsStuned)
	{
		ValueSprint = Value;
		if (ValueX == 0 && ValueY == 0) {}
		else
		{
			if (bIsSprint == true)
			{
				if (ValueSprint == 1)
				{
					bIsAim = false;
					if (SumSprint == 0)
					{
						SumSprint = 1;
						SumWalk = 0;
						SumRun = 0;
						ValueSprint = 0;
						ChangeMovementState(EMovementState::Sprint_State);
						if (CurrentWeapon && CurrentWeapon->bIsWeaponReloading)
						{
							CurrentWeapon->CancelReload();
						}
					}
					if (SumSprint == 2)
					{
						SumSprint = 3;
					}
				}
				else if (ValueSprint == 0)
				{
					if (SumSprint == 1)
					{
						SumSprint = 2;
					}
					else if (SumSprint == 3)
					{
						SumSprint = 0;
						ChangeMovementState(EMovementState::Run_State);
					}
				}
			}
		}
	}
}

void ATPSCharacter::InputWalk(float Value)
{
	if (!bIsStuned)
	{
		ValueWalk = Value;
		if (ValueWalk == 1)
		{
			if (SumWalk == 0)
			{
				SumRun = 0;
				SumWalk = 1;
				SumSprint = 0;
				ValueSprint = 0;
				if (SumAim == 0)
				{
					ChangeMovementState(EMovementState::Walk_State);
				}
				else
				{
					ChangeMovementState(EMovementState::WalkAim_State);
					bIsAim = true;
				}
			}
			if (SumWalk == 2)
			{
				SumWalk = 3;
			}
		}
		else if (ValueWalk == 0)
		{
			if (SumWalk == 1)
			{
				SumWalk = 2;
			}
			else if (SumWalk == 3)
			{
				SumWalk = 0;
				ChangeMovementState(EMovementState::Run_State);
				bIsAim = false;
			}
		}
	}

}

void ATPSCharacter::InputAim(float Value)
{
	if (!bIsStuned)
	{
		ValueAim = Value;
		if (ValueAim == 1)
		{
			if (SumAim == 0)
			{
				SumSprint = 0;
				SumAim = 1;
				SumRun = 0;
				if (SumWalk == 0)
				{
					ChangeMovementState(EMovementState::Aim_State);
				}
				else
				{
					ChangeMovementState(EMovementState::WalkAim_State);
				}
				bIsAim = true;
			}
		}
		else
		{
			if (SumAim == 1)
			{
				if (SumWalk != 0)
				{
					ChangeMovementState(EMovementState::Walk_State);
				}
				else
				{
					ChangeMovementState(EMovementState::Run_State);
				}
				bIsAim = false;
				SumAim = 0;
			}

		}
	}

}

void ATPSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	//CharacterUpdate();
	SetMovementState_OnServer(NewMovementState);

	if (CurrentWeapon)
	{
		CurrentWeapon->UpdateStateWeapon_OnServer(NewMovementState);
	}
}

void ATPSCharacter::MovementTick(float DeltaTime)
{	
	if (bIsAlive && !bIsStuned)
	{
		if(GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), ValueX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), ValueY);

			/*FString SEnum = UEnum::GetValueAsString(MovementState);
			UE_LOG(LogTPS_Net, Warning, TEXT("MovementState - %s"), *SEnum);*/

			APlayerController* PC = Cast<APlayerController>(GetController());
			if (PC)
			{
				FHitResult ResultHit;

				PC->GetHitResultUnderCursor(ECC_GameTraceChannel2, false, ResultHit);
				FVector CursorFV = ResultHit.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();
				FVector HitLocation = ResultHit.Location;
				if (Cursor)
				{
					Cursor->SetWorldRotation(CursorR);
					Cursor->SetWorldLocation(HitLocation);			
					CursorLocation = Cursor->GetComponentLocation();
				}

				if (MovementState != EMovementState::Sprint_State)
				{
					float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CursorLocation).Yaw;
					SetActorRotation(FQuat(FRotator(0, FindRotatorResultYaw, 0)));
					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);
				}
				else
				{
					FVector RotationVector = FVector(ValueX, ValueY, 0.0f);
					FRotator Rotation = RotationVector.ToOrientationRotator();
					SetActorRotation(FQuat(Rotation));
					SetActorRotationByYaw_OnServer(Rotation.Yaw);
				}



				if (CurrentWeapon)
				{	
					switch (MovementState)
					{
					case EMovementState::Aim_State:
						Displacement = FVector(0, 0, 160);
						break;
					case EMovementState::WalkAim_State:
						Displacement = FVector(0, 0, 120);
						break;
					case EMovementState::Run_State:
						Displacement = FVector(0, 0, 120);
						break;
					case EMovementState::Walk_State:
						Displacement = FVector(0, 0, 100);
						break;
					default:
						break;
					}	
					HitLocation.Z = 100;
					
					bool bIsReduceDisplacement = false;
					if (FMath::IsNearlyZero(GetVelocity().Size(), 0.01f))
					{
						//CurrentWeapon->bShouldReduceDispersion = true;
						bIsReduceDisplacement = true;
					}
					else
					{
						//CurrentWeapon->bShouldReduceDispersion = false;
						bIsReduceDisplacement = false;
					}
					CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(HitLocation, bIsReduceDisplacement, Displacement);
				}	
				bIsCharacterStanding = ValueX == 0 && ValueY == 0;
			}
		}

	}

}

void ATPSCharacter::SprintTick(float DeltaTime)
{
	if(!bIsStuned)
	{
		if (SumSprint != 0)
		{
			if (bIsCharacterStanding)
			{
				bIsSprint = false;
				SumSprint = 0;
				ChangeMovementState(EMovementState::Run_State);
			}
			SprintTimer -= DeltaTime;
			if (SprintTimer <= 0)
			{
				SumSprint = 0;
				ChangeMovementState(EMovementState::Run_State);
				bIsSprint = false;
			}
		}
		else if (SumSprint == 0)
		{
			if (SprintTimer < 5)
			{
				SprintTimer += DeltaTime;
				if (SprintTimer <= 5)
				{
					bIsSprint = true;
				}
			}
		}
	}
}

void ATPSCharacter::SpawnTank()
{
	Tank = GetWorld()->SpawnActor<ATank>(TankClass, FTransform(FVector(-660, 1130, 100)));
	if(IsValid(Tank))
	{
		Tank->TankOwner = this;
		Tank->SetActorRotation(FRotator(0, 180, 0));
	}
}

void ATPSCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int8 Random = FMath::RandHelper(DeadAnims.Num());
	if (DeadAnims.IsValidIndex(Random) && DeadAnims[Random] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadAnims[Random]->GetPlayLength();
		//GetMesh()->GetAnimInstance()->Montage_Play(DeadAnims[Random]);
		SetAnim_Multicast(DeadAnims[Random]);
	}
	bIsAlive = false;
	GetWorldTimerManager().SetTimer(TimerHandle_RagdollTimer, this, &ATPSCharacter::EnableRagdoll_Multicast, TimeAnim - 0.02f, false);
	GetWorldTimerManager().SetTimer(TimerHandle_RagdollTimer, this, &ATPSCharacter::EnableRagdoll_ByTimer, TimeAnim, false);
	GetCapsuleComponent()->SetCollisionProfileName(FName("NoCollision"));

	if(CurrentWeapon)
	{
		CurrentWeapon->bCanWeaponFire = false;
	}
}

void ATPSCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetMesh()->SetSimulatePhysics(true);
		GetMesh()->SetCollisionProfileName(FName("Ragdoll"));
	}	
	Cursor->SetVisibility(false);
}

void ATPSCharacter::OverlapReaction(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (OtherActor == Tank)
	{
		MoveWithTank();
	}
}

void ATPSCharacter::MoveWithTank()
{
	if(bIsOverlapTank == true)
	{
		FVector Vector = FVector(Tank->GetActorLocation());
		SetActorLocation(FVector(Vector.X -200, Vector.Y, 191));
		GateArray[0]->MoveName = FName("Open");
		GateArray[0]->bCanSetGateLocation = true;
		bIsInput = false;
		CameraBoom->TargetArmLength = 1500.f;
		Cursor->SetWorldLocation(FVector(0,0,0));
		Tank->Effects(true);
	}
}

//void ATPSCharacter::SpawnGate()
//{
//	Gate = GetWorld()->SpawnActor<AGate>(GateClass, FTransform(FVector(-1390, 1110, 100)));
//	if (IsValid(Gate))
//	{
//		Gate->SetActorRotation(FRotator(0, 90, 0));
//		Gate->Character = this;
//		GateArray.Add(Gate);
//	}
//	Gate = GetWorld()->SpawnActor<AGate>(GateClass, FTransform(FVector(-4470, 1110, 100)));
//	if (IsValid(Gate))
//	{
//		Gate->SetActorRotation(FRotator(0, 90, 0));
//		Gate->Character = this;
//		GateArray.Add(Gate);
//	}
//}

void ATPSCharacter::SpawnCursor()
{
	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if(CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
			Cursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}

}

void ATPSCharacter::InitWeapon(FName NameWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewWeaponIndex)
{
	//OnServer

	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTPSGameInstance* GI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo WeaponInfo;
	if (GI)
	{
		if(GI->GetWeaponInfoByName(NameWeapon, WeaponInfo))
		{
			if (WeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* Weapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (Weapon != nullptr)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					Weapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = Weapon;

					Weapon->WeaponSettings = WeaponInfo;
					Weapon->WeaponInit(WeaponInfo);
					Weapon->ReloadTime = WeaponInfo.ReloadTime;
					Weapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					Weapon->SetInstigator(this);

					CurrentIndexWeapon = NewWeaponIndex;
					Weapon->UpdateStateWeapon_OnServer(MovementState);

					Weapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart_OnServer);
					Weapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd_OnServer);					
					Weapon->OnWeaponFire.AddDynamic(this, &ATPSCharacter::WeaponFire_OnServer);

					if (bSwitchWeaponTimer)
					{
						Weapon->bCanWeaponFire = false;
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon have not found in table - NULL"));
		}
	}
}

void ATPSCharacter::Fire(float Value)
{
	bool bIsFire = Value == 1;
	if (CurrentWeapon)
	{
		CurrentWeapon->SetWeaponStateFire_OnServer(bIsFire, false);
	}
}

void ATPSCharacter::ReloadWeapon_OnServer_Implementation()
{
	if (!bIsStuned && bIsAlive)
	{
		if (CurrentWeapon)
		{
			if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSettings.MaxRound)
			{
				if (!CurrentWeapon->bIsWeaponReloading && CurrentWeapon->CanWeaponReload())
				{
					if(MovementState == EMovementState::Sprint_State)
					{
						SumSprint = 0;
						SetMovementState_OnServer(EMovementState::Run_State);
					}
					CurrentWeapon->InitReload_OnServer();
				}
			}
		}
	}
}

AActor* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPSCharacter::TankFinish()
{
	bIsOverlapTank = false;
	FVector Vector = FVector(Tank->GetActorLocation());
	SetActorLocation(FVector(Vector.X, Vector.Y - 250, 191));
	CameraBoom->TargetArmLength = 800.0f;
	bIsInput = true;
	TopLimit = -4500.0f;
	LeftLimit = -80.0f;
	DownLimit = -6960.0f;
	RightLimit = 2300.0f;
	Tank->Effects(false);
}

void ATPSCharacter::WeaponReloadStart_OnServer_Implementation()
{
	UAnimMontage* Anim = nullptr;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharReloadIronsights;
		break;
	case EMovementState::Walk_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharReloadHip;
		break;
	case EMovementState::Run_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharReloadHip;
		break;
	case EMovementState::WalkAim_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharReloadIronsights;
		break;
	default:
		break;
	}
	SetAnim_Multicast(Anim);
	bIsMagazinReload = true;
	bCanAttachMagazin = true;
	bCanEjectionMagazin = true;
    bCanEndReloadMagazin = true;

	WeaponReloadStart_Multicast();
}

void ATPSCharacter::WeaponReloadEnd_OnServer_Implementation(bool bIsSuccess, int32 CoutChangeAmmo)
{
	bIsMagazinReload = false;

	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->WeaponChangeAmmo(CurrentWeapon->WeaponSettings.WeaponType, CoutChangeAmmo);		
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	if(CurrentWeapon)
	{
		WeaponReloadEnd_Multicast(CurrentWeapon->WeaponSettings.BoneName, bIsSuccess, CoutChangeAmmo);
	}
	bIsMagazinReload = false;
	MagazinReloadTime = 0;
	bCanEndReloadMagazin = false;
}

void ATPSCharacter::StopAnim(UAnimMontage* Anim)
{
	if (Anim)
	{
		StopAnimMontage(Anim);
	}
}

void ATPSCharacter::SetAnim_Multicast_Implementation(UAnimMontage* NewAnim)
{
	if(NewAnim)
	{
		PlayAnimMontage(NewAnim);				
	}
}

void ATPSCharacter::WeaponFire_OnServer_Implementation()
{
	UAnimMontage* Anim = nullptr;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharFireIronsights;
		break;
	case EMovementState::Walk_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharFireHip;
		break;
	case EMovementState::Run_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharFireHip;
		break;
	case EMovementState::WalkAim_State:
		Anim = CurrentWeapon->WeaponSettings.AnimCharFireIronsights;
		break;
	default:
		break;
	}
	
	SetAnim_Multicast(Anim);
	WeaponFire_Multicast();

	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
}

void ATPSCharacter::MagazinReload_OnServer_Implementation(float DeltaTime)
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->MagazinClass)
		{
			if (bIsMagazinReload)
			{
				MagazinReloadTime += DeltaTime;
				FWeaponInfo WeaponInfo = CurrentWeapon->WeaponSettings;	
				
				if (bCanEjectionMagazin)
				{
					if (MagazinReloadTime >= WeaponInfo.TimeToMagazinEjection)
					{
						CurrentWeapon->InitDropMesh_OnServer(WeaponInfo.MagazinDropMesh, CurrentWeapon->MagazinLocation->GetRelativeTransform(), CurrentWeapon->MagazinLocation->GetForwardVector(), WeaponInfo.MagazinLifeTime, WeaponInfo.MagazinDropImpulseDispersion, WeaponInfo.MagazinDropImpulsePower, WeaponInfo.MagazinCustomMass);
						CurrentWeapon->HideBoneByName_Multicast(WeaponInfo.BoneName);
						bCanEjectionMagazin = false;
						WeaponReloadSound_Multicast(WeaponInfo.SoundReloadWeapon);
					}
				}
				if (bCanAttachMagazin)
				{
					if (MagazinReloadTime >= WeaponInfo.AttachMagazinToHandTime)
					{
						InitAttachableMesh_Multicast(WeaponInfo.MagazinDropMesh, FName("MagazinSocketLeftHand"));					
						bCanAttachMagazin = false;
						WeaponReloadSound_Multicast(WeaponInfo.SoundReloadWeapon);
					}
				}
			}
		}
	}
}

void ATPSCharacter::SwitchWeapon_OnServer_Implementation(float Value)
{
	if (!bIsStuned)
	{
		if (Value != 0)
		{
			if(!bIsSwithWeaponButtonPressed)
			{
				bIsSwithWeaponButtonPressed = true;
				if(CurrentWeapon)
				{
					if (CurrentWeapon->bIsWeaponReloading)
					{
						CurrentWeapon->CancelReload();
					}
					InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + Value, CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
				}
			}
		}
		else 
		{
			bIsSwithWeaponButtonPressed = false;
		}
	}
}

void ATPSCharacter::TryUseFirstAidAbility()
{
	if (!bIsStuned)
	{
		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

float ATPSCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if(bIsAlive)
	{
		HealthComponent->ChangeCurrentHealth(-ActualDamage);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* Projectile = Cast<AProjectileDefault>(DamageCauser);
		if (Projectile)
		{
			UTypes::AddEffectBySurfaceType(this, Projectile->ProjectileInfo.Effect, GetSurfaceType(), NAME_None);
		}
	}
	return ActualDamage;
}

EPhysicalSurface ATPSCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (HealthComponent)
	{
		if (GetMesh())
		{
			UMaterialInterface* Material = GetMesh()->GetMaterial(0);
			if (Material)
			{
				Result = Material->GetPhysicalMaterial()->SurfaceType;
			}
		}
	}

	return Result;
}

TArray<UTPS_StateEffect*> ATPSCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATPSCharacter::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATPSCharacter::AddEffect(UTPS_StateEffect* NewEffect)
{	
	Effects.Add(NewEffect);
}

void ATPSCharacter::Stun()
{
	bIsStuned = true;
	if (CurrentWeapon)
	{
		if (bIsMagazinReload)
		{
			CurrentWeapon->CancelReload();

		}
		CurrentWeapon->bCanWeaponFire = false;
	}
	if (bIsSprint)
	{
		bIsSprint = false;
		SumSprint = 0;
		ChangeMovementState(EMovementState::Run_State);
	}

	PlayAnimMontage(StunAnim);
}

void ATPSCharacter::EndStun()
{
	bIsStuned = false;
	if (GetMesh()->GetAnimInstance()->IsAnyMontagePlaying())
	{
		GetMesh()->GetAnimInstance()->StopAllMontages(0.15f);
	}
}

void ATPSCharacter::CharDead_BP_Implementation()
{
	//BP
}

void ATPSCharacter::InitEquipment_Multicast_Implementation(const TArray<FName> &SocketNames, USkeletalMesh* NewMesh, EEquipmentType EquipType)
{
	for (int i = 0; i < EquipmentByType.Num(); i++)
	{
		if (EquipmentByType[i].EquipType == EquipType)
		{
			for (int j = 0; j < EquipmentByType[i].SkeletalMesh.Num(); j++)
			{
				if (EquipmentByType[i].SkeletalMesh[j])
				{
					EquipmentByType[i].SkeletalMesh[j]->Destroy();
				}
			}
		}
	}
	for (int i = 0; i < SocketNames.Num(); i++)
	{
		ASkeletalMeshActor* NewEquipment = nullptr;
		NewEquipment = GetWorld()->SpawnActor<ASkeletalMeshActor>(ASkeletalMeshActor::StaticClass());
		NewEquipment->GetSkeletalMeshComponent()->SetSkeletalMesh(NewMesh);
		NewEquipment->GetSkeletalMeshComponent()->SetCollisionProfileName(FName("NoCollision"));
		FAttachmentTransformRules Rules(EAttachmentRule::SnapToTarget, false);
		if (GetMesh())
		{
			NewEquipment->AttachToComponent(GetMesh(), Rules, SocketNames[i]);
		}

		for (int j = 0; j < EquipmentByType.Num(); j++)
		{
			if (EquipmentByType[j].EquipType == EquipType)
			{
				EquipmentByType[j].SkeletalMesh.Add(NewEquipment);
			}
		}
	}
}

void ATPSCharacter::InitEquipment_OnServer_Implementation(FName NewEquipmentName, FName OldEquipmentName)
{
	//On Server
	UTPSGameInstance* GI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());	
	if (GI)
	{		
		FEquipmentInfo OldEquipInfo;
		if (GI->GetEquipmentInfoByName(OldEquipmentName, OldEquipInfo))
		{
			for (int i = 0; i < OldEquipInfo.BonesToHide.Num(); i++)
			{
				UnHideBoneByName_Multicast(OldEquipInfo.BonesToHide[i]);
			}
			HealthComponent->CoefDamage += OldEquipInfo.Protection* HealthComponent->CoefDamage;
			SpeedBoost -= OldEquipInfo.SpeedBoost;
		}
		FEquipmentInfo NewEquipInfo;
		if(GI->GetEquipmentInfoByName(NewEquipmentName, NewEquipInfo))
		{
			for (int j = 0; j < NewEquipInfo.BonesToHide.Num(); j++)
			{
				HideBoneByName_Multicast(NewEquipInfo.BonesToHide[j]);
			}
			HealthComponent->CoefDamage -= NewEquipInfo.Protection* HealthComponent->CoefDamage;
			SpeedBoost += NewEquipInfo.SpeedBoost;


			InitEquipment_Multicast(NewEquipInfo.SocketNames, NewEquipInfo.SkeletalMesh, NewEquipInfo.DefenceArea);
		}
	}
}

void ATPSCharacter::WeaponReloadEnd_Multicast_Implementation(FName BoneToHide, bool bIsSuccess, int32 AmmoNeedTake)
{
	if(GetMesh())
	{
		GetMesh()->GetAnimInstance()->StopAllMontages(0.15f);
	}
	if(AttachedMagazin)
	{
		AttachedMagazin->Destroy();
	}
	if(CurrentWeapon && CurrentWeapon->SkeletalMeshWeapon)
	{
		CurrentWeapon->SkeletalMeshWeapon->UnHideBoneByName(BoneToHide);
	}
	WeaponReloadEnd_BP(bIsSuccess, AmmoNeedTake);
}

void ATPSCharacter::InitAttachableMesh_Multicast_Implementation(UStaticMesh* NewMesh, FName SocketNameToAttach)
{
	FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
	Rule.KeepWorldTransform;
	
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(FVector(0));
	SpawnTransform.SetRotation(FRotator(0).Quaternion());
	SpawnTransform.SetScale3D(FVector(1));

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), SpawnTransform, SpawnParams);
	if(NewActor)
	{
		if(NewActor->GetStaticMeshComponent())
		{
			NewActor->GetStaticMeshComponent()->SetStaticMesh(NewMesh);
		}
		NewActor->AttachToComponent(GetMesh(), Rule, SocketNameToAttach);
		NewActor->SetLifeSpan(3);
	}
	AttachedMagazin = NewActor;
}

void ATPSCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast_Implementation(Yaw);
}

void ATPSCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATPSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewMovementState)
{
	SetMovementState_Multicast(NewMovementState);
}

void ATPSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

void ATPSCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
	}
}

bool ATPSCharacter::TrySwitchWeaponByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;
			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->bIsWeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
			}
			bIsSuccess = InventoryComponent->SwitchWeaponToIndex(ToIndex, OldIndex, OldInfo);
		}
	}
	return bIsSuccess;
}

void ATPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATPSCharacter, MovementState);
	DOREPLIFETIME(ATPSCharacter, CurrentWeapon);
}

void ATPSCharacter::HideBoneByName_Multicast_Implementation(FName BoneToHide)
{
	if(GetMesh())
	{
		GetMesh()->HideBoneByName(BoneToHide, EPhysBodyOp::PBO_None);
	}
}

void ATPSCharacter::UnHideBoneByName_Multicast_Implementation(FName BoneToUnHide)
{
	if(GetMesh())
	{
		GetMesh()->UnHideBoneByName(BoneToUnHide);
	}
}

void ATPSCharacter::WeaponReloadStart_BP_Implementation()
{
	//In BP
}

void ATPSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess, int32 AmmoNeedTake)
{
	//In BP
}

void ATPSCharacter::WeaponFire_BP_Implementation()
{
	//In BP
}

void ATPSCharacter::WeaponReloadSound_Multicast_Implementation(USoundBase* Sound)
{
	if(Sound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Sound, GetActorLocation());
	}
}

void ATPSCharacter::WeaponReloadStart_Multicast_Implementation()
{
	WeaponReloadStart_BP();
}

void ATPSCharacter::WeaponFire_Multicast_Implementation()
{
	WeaponFire_BP();
}

void ATPSCharacter::CharDead_BP_OnServer_Implementation()
{
	CharDead_BP();
}

void ATPSCharacter::CharDead_BP_ByTimer()
{
	CharDead_BP();
}

void ATPSCharacter::EnableRagdoll_ByTimer()
{
	EnableRagdoll_Multicast();
}
