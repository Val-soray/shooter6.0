// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_IGameActor.h"

// Add default functionality here for any ITPS_IGameActor functions that are not pure virtual.

EPhysicalSurface ITPS_IGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTPS_StateEffect*> ITPS_IGameActor::GetAllCurrentEffects()
{
	TArray<UTPS_StateEffect*> Effects;
	return Effects;
}

void ITPS_IGameActor::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{
}

void ITPS_IGameActor::AddEffect(UTPS_StateEffect* NewEffect)
{
}

void ITPS_IGameActor::Stun()
{
}

void ITPS_IGameActor::EndStun()
{
}

void ITPS_IGameActor::Invulnerable()
{
	bIsInvulnerable = true;
}

void ITPS_IGameActor::EndInvulnerable()
{
	bIsInvulnerable = false;
}

bool ITPS_IGameActor::IsInvulnerable()
{
	return bIsInvulnerable;
}
