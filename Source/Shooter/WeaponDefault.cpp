// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "ProjectileDefault.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "SleeveBullets.h"
#include "Magazin.h"
#include "Kismet/GameplayStatics.h"
#include "TPSInventoryComponent.h"
#include "TPS_IGameActor.h"
#include "TPS_StateEffect.h"
#include "Types.h"
#include "TPSCharacter.h"
#include "Perception/AISense_Damage.h"
#include "Net/UnrealNetwork.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.

	SetReplicates(true);
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	SleeveLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("SleeveLocation"));
	SleeveLocation->SetupAttachment(RootComponent);

	MagazinLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("MagazinLocation"));
	MagazinLocation->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(HasAuthority())
	{
		FireTick(DeltaTime);

		ReloadTick(DeltaTime);

		DispersionTick(DeltaTime);
	}

}

void AWeaponDefault::WeaponInit(FWeaponInfo Info)
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bIsWeaponReloading)
	{
		if (ReloadTimer <= 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}

}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if(bCanWeaponFire)
	{
		if (GetWeaponRound() > 0)
		{		
			if (bIsWeaponFiring == true)
			{
				if (FireTimer <= 0.0f)
				{
					if (bIsWeaponReloading)
					{
						CancelReload();
					}
					SpawnBullet();
					FireTimer = WeaponSettings.RateOfFire;	
				}
			}
		}
		else if (!bIsWeaponReloading)
		{
			if(CanWeaponReload())
			{
				InitReload_OnServer();
			}
			else
			{
				UTPSInventoryComponent* InventoryComp = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
				if(InventoryComp)
				{
					ATPSCharacter* Char = Cast<ATPSCharacter>(InventoryComp->GetOwner());
					if(Char)
					{
						InventoryComp->WeaponAmmoEmpty_Multicast(Char->CurrentIndexWeapon, true);
						//InventoryComp->OnWeaponAmmoEmpty.Broadcast(Char->CurrentIndexWeapon, true);
						bIsAmmoEmpty = true;
						InventoryComp->SwitchWeaponToIndex(Char->CurrentIndexWeapon + 1, Char->CurrentIndexWeapon, AdditionalWeaponInfo);
					}
				}
			}
		}
		
		if (FireTimer > 0)
		{
			FireTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::SpawnBullet()
{
	//On Server
	FVector SpawnLocation = ShootLocation->GetComponentLocation();
	FRotator SpawnRotation = ShootLocation->GetComponentRotation();
	FProjectileInfo ProjectileInfo = WeaponSettings.ProjectileSettings;

	int8 NumberProjectile = GetNumberProjectileByShot();
	for(int8 i = 0; i < NumberProjectile; i++)
	{
		if (ProjectileInfo.Projectile)
		{	
			FVector Direction = GetFireEndLocation() - SpawnLocation;
			Direction.Normalize();
			FMatrix Matrix;
			//if (WeaponSettings.WeaponName != FName("GrenadeLauncher_v1"))
			
			Matrix = FMatrix(Direction, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);	
			SpawnRotation = Matrix.Rotator();
			

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = GetInstigator();

			AProjectileDefault* Bullet = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
			if (Bullet)
			{
				Bullet->InitProjectile(WeaponSettings.ProjectileSettings);
			}

		}
		else
		{
			TArray<AActor*> IgnorActors;
			FHitResult HitResult;

			UKismetSystemLibrary::LineTraceSingle
			(
				GetWorld(),
				SpawnLocation,
				GetFireEndLocation(),
				ETraceTypeQuery::TraceTypeQuery3,
				false,
				IgnorActors,
				EDrawDebugTrace::ForDuration,
				HitResult,
				true,
				FLinearColor::Yellow,
				FLinearColor::Red,
				1
			);

			if(HitResult.bBlockingHit)
			{
				if (HitResult.Actor.IsValid() && HitResult.PhysMaterial.IsValid())
				{
					EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(HitResult);

					if (ProjectileInfo.HitDecals.Contains(SurfaceType))
					{
						UMaterialInterface* Material = ProjectileInfo.HitDecals[SurfaceType];

						if (Material)
						{
							SpawnTraceHitDecal_Multicast(Material, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), 10.0f);
						}
					}
					if (ProjectileInfo.HitFXs.Contains(SurfaceType))
					{
						UParticleSystem* FX = ProjectileInfo.HitFXs[SurfaceType];

						if (FX)
						{
							SpawnTraceHitFX_Multicast(FX, HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(0.5f));
						}
					}
					if (ProjectileInfo.HitSounds.Contains(SurfaceType))
					{
						USoundBase* Sound = ProjectileInfo.HitSounds[SurfaceType];
						if (Sound)
						{
							SpawnTraceHitSound_Multicast(Sound, HitResult.ImpactPoint);
						}
					}

					float Distance = FVector(SpawnLocation - HitResult.ImpactPoint).Size();
					float Damage = ProjectileInfo.ProjectileDamage;
					if (Distance < WeaponSettings.DistanceTrace)
					{
						if (Distance > WeaponSettings.DistanceTrace * 0.1)
						{
							float Percent = Distance / WeaponSettings.DistanceTrace;
							if (Damage * Percent < Damage * 0.5)
							{
								Damage -= Damage * Percent;
							}
							else
							{
								Damage = ProjectileInfo.ProjectileDamage * 0.5;
							}
						}
					}
					else
					{
						Damage = 0;
					}

					UTypes::AddEffectBySurfaceType(HitResult.GetActor(), ProjectileInfo.Effect, SurfaceType, HitResult.BoneName);
					
					UGameplayStatics::ApplyDamage(HitResult.GetActor(), Damage, GetInstigatorController(), this, NULL);

					UAISense_Damage::ReportDamageEvent(GetWorld(), HitResult.GetActor(), GetInstigator(), Damage, GetActorLocation(), HitResult.Location);
				}
			}
		}
	}

	
	ChangeDispersionByShot();
	AdditionalWeaponInfo.Round--;
	InitDropMesh_OnServer(WeaponSettings.SleeveBulletsMesh, SleeveLocation->GetRelativeTransform(), SleeveLocation->GetForwardVector(), WeaponSettings.SleeveLifeTime, WeaponSettings.SleeveDropImpulseDispersion, WeaponSettings.SleeveDropImpulsePower, WeaponSettings.SleeveCustomMass);
	FXWeaponFire_Multicast(WeaponSettings.FireWeaponEffect, WeaponSettings.SoundFireWeapon);
	if(WeaponSettings.AnimWeaponFire)
	{
		WeaponFireAnim_Muticast(WeaponSettings.AnimWeaponFire);
	}
	OnWeaponFire.Broadcast();
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload_OnServer_Implementation()
{
	bIsWeaponReloading = true;
	SetWeaponStateFire_OnServer(false, true);
	ReloadTimer = WeaponSettings.ReloadTime;

	OnWeaponReloadStart.Broadcast();
	
}

void AWeaponDefault::FinishReload()
{
	int8 AvailableAmmoForReload = GetAvailableAmmoForReload();

	bIsWeaponReloading = false;
	TimeToEjection = WeaponSettings.TimeToMagazinEjection;

	int32 CoutChangeAmmo = AdditionalWeaponInfo.Round - AvailableAmmoForReload;

	AdditionalWeaponInfo.Round = AvailableAmmoForReload;

	OnWeaponReloadEnd.Broadcast(true, CoutChangeAmmo);
}

void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	switch (NewMovementState)
	{
	case EMovementState::Aim_State:

		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Aim_StateDispersionReduction;

		bCanWeaponFire = true;

		break;

	case EMovementState::WalkAim_State:

		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.AimWalk_StateDispersionReduction;

		bCanWeaponFire = true;

		break;

	case EMovementState::Walk_State:

		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Walk_StateDispersionReduction;

		bCanWeaponFire = true;

		break;

	case EMovementState::Run_State:

		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Run_StateDispersionReduction;		

		bCanWeaponFire = true;

		break;

	case EMovementState::Sprint_State:

		SetWeaponStateFire_OnServer(false, true);
		bCanWeaponFire = false;

		break;

	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersion()
{
}

FVector AWeaponDefault::GetFireEndLocation() const
{	
	FVector EndLocation = FVector(0);

	FVector ShootDistance = ShootLocation->GetComponentLocation() - ShootEndLocation;
	if(ShootDistance.Size() > 100)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
	}
		
	//DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -DirectionShoot)
	return EndLocation;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{	
	FVector ConeVector = FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
	return ConeVector;

}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result;

	Result = CurrentDispersion;

	return CurrentDispersion;
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFiring, bool bIsResetFireTimer)
{
	bIsWeaponFiring = bIsFiring;
	if (bIsResetFireTimer == true)
	{
		FireTimer = 0.0f;
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (bShouldReduceDispersion)
	{
		CurrentDispersion -= CurrentDispersionReduction * DeltaTime;
	}
	else
	{
		CurrentDispersion += CurrentDispersionReduction * DeltaTime;
	}

	if (CurrentDispersion > CurrentDispersionMax)
	{
		CurrentDispersion = CurrentDispersionMax;
	}
	if (CurrentDispersion < CurrentDispersionMin)
	{
		CurrentDispersion = CurrentDispersionMin;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform DropOffset, FVector DropImpulseDirection, float LifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	//On Server
	FVector LocalDirection = this->GetActorForwardVector()* DropOffset.GetLocation().X + this->GetActorForwardVector() * DropOffset.GetLocation().Y + this->GetActorForwardVector() * DropOffset.GetLocation().Z;
	FTransform SpawnTransform; 
	SpawnTransform.SetLocation(GetActorLocation() + DropOffset.GetLocation());
	SpawnTransform.SetRotation((GetActorRotation() + DropOffset.Rotator()).Quaternion());
	SpawnTransform.SetScale3D(DropOffset.GetScale3D());

	InitDropMesh_Multicast(DropMesh, SpawnTransform, DropImpulseDirection, LifeTime, ImpulseRandomDispersion, PowerImpulse, CustomMass, LocalDirection);
}

void AWeaponDefault::InitDropMesh_Multicast_Implementation(UStaticMesh* DropMesh, FTransform DropOffset, FVector DropImpulseDirection, float LifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDirection)
{
	FActorSpawnParameters SpawnParam;
	SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnParam.Owner = this;
	AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), DropOffset, SpawnParam);
	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnorOnlyPawn"));
		NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		NewActor->SetActorTickEnabled(true);
		NewActor->InitialLifeSpan = LifeTime;
		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_EngineTraceChannel1, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_EngineTraceChannel2, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Block);

		if (CustomMass > 0.0f)
		{
			NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
		}
		if (!DropImpulseDirection.IsNearlyZero())
		{
			FVector FinalDirection;
			LocalDirection += DropImpulseDirection * 1000;
			if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
			{
				FinalDirection = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDirection, ImpulseRandomDispersion);
			}	
			NewActor->GetStaticMeshComponent()->AddImpulse(FinalDirection * PowerImpulse);
		}
	}
}

void AWeaponDefault::CancelReload()
{
	bIsWeaponReloading = false;

	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd.Broadcast(false, 0);
}

bool AWeaponDefault::CanWeaponReload()
{
	bool Result = true;
	if (GetOwner())
	{
		UTPSInventoryComponent* InventoryComp = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (InventoryComp)
		{
			int32 AvaibleAmmoForWeapon = 0;
			if (!InventoryComp->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvaibleAmmoForWeapon))
			{
				Result = false;
			}
		}
	}

	return Result;
}

int32 AWeaponDefault::GetAvailableAmmoForReload()
{
	int32 AmmoForWeapon = WeaponSettings.MaxRound;

	if (GetOwner())
	{
		UTPSInventoryComponent* InventoryComp = Cast<UTPSInventoryComponent>(GetOwner()->GetComponentByClass(UTPSInventoryComponent::StaticClass()));
		if (InventoryComp)
		{
			int32 AvaibleAmmoForWeapon = 0;
			if (InventoryComp->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvaibleAmmoForWeapon))
			{
				if(AvaibleAmmoForWeapon + AdditionalWeaponInfo.Round < WeaponSettings.MaxRound)
				{
					AmmoForWeapon = AvaibleAmmoForWeapon + AdditionalWeaponInfo.Round;
				}			
			}
		}
	}

	return AmmoForWeapon;
}

void AWeaponDefault::WeaponFireAnim_Muticast_Implementation(UAnimMontage* FireAnim)
{
	if (FireAnim && SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(FireAnim);
	}
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool NewShouldReduceDispersion, FVector Displacement)
{
	if (WeaponSettings.bShouldUseDisplacement)
	{
		ShootEndLocation = NewShootEndLocation + Displacement;
	}
	else
	{
		ShootEndLocation = NewShootEndLocation;
	}
	bShouldReduceDispersion = NewShouldReduceDispersion;
}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);
	DOREPLIFETIME(AWeaponDefault, bIsWeaponReloading);
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FireFX, USoundBase* FireSound)
{
	if(FireFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireFX, ShootLocation->GetComponentTransform());
	}
	if(FireSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), FireSound, ShootLocation->GetComponentLocation());
	}
}

void AWeaponDefault::HideBoneByName_Multicast_Implementation(FName BoneToHide)
{
	SkeletalMeshWeapon->HideBoneByName(BoneToHide, EPhysBodyOp::PBO_None);
}

void AWeaponDefault::SpawnTraceHitDecal_Multicast_Implementation(UMaterialInterface* Material, FVector Location, FRotator Rotation, float Size)
{
	UGameplayStatics::SpawnDecalAtLocation(GetWorld(), Material, FVector(10), Location, Rotation, Size);
}

void AWeaponDefault::SpawnTraceHitFX_Multicast_Implementation(UParticleSystem* FX, FRotator Rotation, FVector Location, FVector Size)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, FTransform(Rotation, Location, Size));
}

void AWeaponDefault::SpawnTraceHitSound_Multicast_Implementation(USoundBase* Sound, FVector Location)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Location);
}
