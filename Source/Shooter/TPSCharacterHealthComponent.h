// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class SHOOTER_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()
	
public:

	FTimerHandle TimerHandle_CoolDownShield;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
	FOnShieldChange OnShieldChange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryCooldownTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryRate = 0.1f;

protected:

	float Shield = 100.0f;
	float HealthBoost = 0;

public:

	void ChangeCurrentHealth(float ChageValue) override;
	void ChangeShieldValue(float ShieldValue);
	UFUNCTION(BlueprintCallable)
	float GetCurrentShield();
	void ShieldCooldownEnd();
	void RecoveryShield();
	float GetHealthBoost();
	void SetHealthBoost(float Boost);

	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_Multicast(float NewShield, float ChandeValue);
};
