// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSCharacterHealthComponent.h"

void UTPSCharacterHealthComponent::ChangeCurrentHealth(float ChangeValue)
{
	if(IsCanChangeHealth(ChangeValue))
	{
		if (GetHealthBoost() > 0 && ChangeValue < 0)
		{
			SetHealthBoost(ChangeValue);
		}
		else
		{
			float CurrentDamage = ChangeValue;
			CurrentDamage = ChangeValue * CoefDamage;
			if (ChangeValue < 0 && GetCurrentShield() > 0)
			{
				ChangeShieldValue(CurrentDamage);
			}
			else
			{
				Super::ChangeCurrentHealth(ChangeValue);
			}
		}
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ShieldValue)
{
	Shield += ShieldValue;
	ShieldChangeEvent_Multicast(Shield, ShieldValue);
	//OnShieldChange.Broadcast(Shield, ShieldValue);
	if (Shield >= 100)
	{
		Shield = 100;
	}
	else if (Shield <= 0)
	{
		Shield = 0;
	}	

	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShield, this, &UTPSCharacterHealthComponent::ShieldCooldownEnd, ShieldRecoveryCooldownTime, false);
}
void UTPSCharacterHealthComponent::ShieldCooldownEnd()
{
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShield, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float NewShield = Shield;
	NewShield += ShieldRecoveryValue;

	if (NewShield >= 100)
	{
		Shield = 100;
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}	
	else
	{
		Shield = NewShield;
	}
	ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);
	//OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}

float UTPSCharacterHealthComponent::GetHealthBoost()
{
	return HealthBoost;
}

void UTPSCharacterHealthComponent::SetHealthBoost(float Boost)
{
	HealthBoost += Boost;
	if (HealthBoost < 0)
	{
		HealthBoost = 0;
	}
}

void UTPSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float ChandeValue)
{
	OnShieldChange.Broadcast(NewShield, ChandeValue);
}
