// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS_IGameActor.h"

#include "CharacterEnemy_Base.generated.h"

class UTPSHealthComponent;

using namespace UF;
using namespace UP;

UCLASS()
class SHOOTER_API ACharacterEnemy_Base : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterEnemy_Base();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		TArray<UTPS_StateEffect*> Effects;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UTPSHealthComponent* Health = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		EPhysicalSurface GetSurfaceType() override;
	//Effects
	UFUNCTION(BlueprintCallable)
		TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	UFUNCTION(BlueprintCallable)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
	void AddEffect(UTPS_StateEffect* NewEffect) override;

};
