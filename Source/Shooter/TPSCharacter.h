// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "TPS_IGameActor.h"
#include "Animation/SkeletalMeshActor.h"
#include "Engine/StaticMeshActor.h"

#include "TPSCharacter.generated.h"

class ATank;
class AGate;
class AWeaponDefault;
class AMagazin;
class UTPSInventoryComponent;
class UTPSCharacterHealthComponent;
class UTPS_StateEffect;

using namespace UF;
using namespace UP;

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()

public:

	ATPSCharacter();

	//Classes
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ATank>TankClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGate>GateClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UTPS_StateEffect>AbilityEffect;

	//Arrays
	UPROPERTY(BlueprintReadWrite)
	TArray<AGate*>GateArray;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dead")
	TArray<UAnimMontage*> DeadAnims;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<UTPS_StateEffect*> Effects;

	//Timers
	FTimerHandle TimerHandle_RagdollTimer;

	//References
	AGate* Gate = nullptr;
	ATank* Tank = nullptr;
	UDecalComponent* Cursor = nullptr;
	AWeaponDefault* WeaponDefault = nullptr;
	AStaticMeshActor* AttachedMagazin = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Weapon")
	AWeaponDefault* CurrentWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UAnimMontage* SwitchWeaponAnim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stun")
	UAnimMontage* StunAnim = nullptr;

	//Names
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName WeaponName;

	//float Variables		
	UPROPERTY(BlueprintReadWrite)
	float CameraHigh = 700.f;
	UPROPERTY(BlueprintReadWrite)
	float SprintTimer = 5;
	float TankTimer = 2;
	float ValueAim = 0;
	float ValueWalk = 0;
	float ValueSprint = 0;
	float ValueX = 0;
	float ValueY = 0;
	float ValueFire = 0;
	float LeftLimit = -1745.0f;
	float RightLimit = 1905.0f;
	float TopLimit = 2305.0f;
	float DownLimit = -1345.0f;
	float HighLimit = 100.0f;
	float SwitchWeaponTime = 0;	
	float ResSpeed = 600;
	float MagazinReloadTime = 0;
	UPROPERTY(Replicated)
	float SpeedBoost = 0;


	//int32 Variables
	int32 SumWalk = 0;
	int32 SumSprint = 0;
	int32 SumAim = 0;
	int32 SumRun = 0;
	int32 SumFire = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int32 CurrentIndexWeapon = 0;
	UPROPERTY(EditDefaultsOnly)
	int32 SumWeaponSlots = 5;

	//bool variables	
	UPROPERTY(BlueprintReadOnly)
	bool bIsAim = false;	
	bool bIsOverlapTank = true;
	bool bIsStandCrouchAnim = false;
	bool bIsSprint = true;	
	bool bCanAttachMagazin = true;
	bool bCanEjectionMagazin = true;
	bool bCanEndReloadMagazin = true;
	bool bIsMagazinReload = false;	
	bool bIsRememberLocation = true;
	bool bCanSwitchWeaponInTimer = true;
	bool bSwitchWeaponTimer = false;
	bool bIsCharacterStanding = false;
	UPROPERTY(BlueprintReadOnly)
	bool bIsInput = true;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dead")
	bool bIsAlive = true;
	bool bIsSwithWeaponButtonPressed = false;
	bool bIsWeaponSwitching = false;

	//Vector Variables
	UPROPERTY(BlueprintReadOnly)
	FVector CursorLocation = FVector(0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	FVector CursorSize = FVector(20, 40, 40);
	FVector Displacement = FVector(0);

	//Enums
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	//FORCEINLINEs
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE class UCapsuleComponent* GetCapsule() const { return Capsule; }

	UPROPERTY(EditDefaultsOnly, Category = "Equipment")
	TArray<FEquipmentByType> EquipmentByType;

private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* Capsule;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UTPSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UTPSCharacterHealthComponent* HealthComponent;

public:	

	//Override Functions
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;
	virtual void BeginPlay() override;
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	//Input Functions
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void InputSprint(float Value);
	UFUNCTION()
	void InputWalk(float Value);
	UFUNCTION()
	void InputAim(float Value);		
	UFUNCTION(BlueprintCallable)
	void Fire(float Value);	
	UFUNCTION(Server, Reliable)
	void SwitchWeapon_OnServer(float Value);
	UFUNCTION()
	void TryUseFirstAidAbility();
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ReloadWeapon_OnServer();

	UFUNCTION()
	void MovementTick(float DeltaTime);
	UFUNCTION(NetMulticast, Reliable)
	void SetAnim_Multicast(UAnimMontage* NewAnim);
	UFUNCTION()
	void StopAnim(UAnimMontage* Anim);
	//Weapon
	UFUNCTION(Server, Reliable)
	void WeaponReloadStart_OnServer();
	UFUNCTION(Server, Reliable)
	void WeaponReloadEnd_OnServer(bool bIsSuccess, int32 AmmoNeedTake);
	UFUNCTION(Server, Reliable)
	void WeaponFire_OnServer();


	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP();
	UFUNCTION(NetMulticast, Unreliable)
	void WeaponReloadStart_Multicast();

	UFUNCTION(NetMulticast, Reliable)
	void WeaponReloadEnd_Multicast(FName BoneToHide, bool bIsSuccess, int32 AmmoNeedTake);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess, int32 AmmoNeedTake);

	UFUNCTION(NetMulticast, Unreliable)
	void WeaponFire_Multicast();
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFire_BP();


	UFUNCTION(Server, Unreliable)
	void MagazinReload_OnServer(float DeltaTime);
	UFUNCTION()
	bool TrySwitchWeaponByKeyInput(int32 ToIndex);

	UFUNCTION()
	void MoveWithTank();
	//UFUNCTION()
	//void SpawnGate();
	UFUNCTION()
	void SpawnCursor();
	UFUNCTION()
	void SprintTick(float DeltaTime);
	UFUNCTION()
	void SpawnTank();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();
	UFUNCTION()
	void OverlapReaction(UPrimitiveComponent* OverlappedComponent,AActor* OtherActor,UPrimitiveComponent* OtherComp,int32 OtherBodyIndex,bool bFromSweep,const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState(EMovementState NewMovementState);
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName NameWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewWeaponIndex);
	UFUNCTION(BlueprintCallable)
	AActor* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void TankFinish();	
	//Dead
	UFUNCTION(BlueprintCallable)
	void CharDead();
	UFUNCTION()
	void DropCurrentWeapon();

	//Interface
	UFUNCTION(BlueprintCallable)
	EPhysicalSurface GetSurfaceType() override;
		//Effects
		UFUNCTION(BlueprintCallable)
		TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
		UFUNCTION(BlueprintCallable)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
		void AddEffect(UTPS_StateEffect* NewEffect) override;
		void Stun() override;
		void EndStun() override;

	//template
	template <int32 Id> void TKeyPressed()
	{
		TrySwitchWeaponByKeyInput(Id);
	}
	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();

	UFUNCTION(Server, Reliable, BlueprintCallable)
		void InitEquipment_OnServer(FName NewEquipmentName, FName OldEquipmentName);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void InitEquipment_Multicast(const TArray<FName> &SocketNames, USkeletalMesh* NewMesh, EEquipmentType EquipType);

	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewMovementState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewMovementState);

	UFUNCTION(NetMulticast, Reliable)
	void InitAttachableMesh_Multicast(UStaticMesh* NewMesh, FName SocketNameToAttach);

	UFUNCTION(NetMulticast, Reliable)
	void HideBoneByName_Multicast(FName BoneToHide);

	UFUNCTION(NetMulticast, Reliable)
	void UnHideBoneByName_Multicast(FName BoneToUnHide);

	UFUNCTION(NetMulticast, Unreliable)
	void WeaponReloadSound_Multicast(USoundBase* Sound);

	UFUNCTION(Server, Reliable)
	void CharDead_BP_OnServer();

	UFUNCTION()
	void CharDead_BP_ByTimer();

	void EnableRagdoll_ByTimer();
};