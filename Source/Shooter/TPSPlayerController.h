// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TPSPlayerController.generated.h"

class ATPSCharacter;

UCLASS()
class SHOOTER_API ATPSPlayerController : public APlayerController
{
	GENERATED_BODY()
	

public:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ATPSCharacter>CharacterClass;

	UPROPERTY(BlueprintReadOnly)
		ATPSCharacter* TPSCharacter = nullptr;

	virtual void OnUnPossess() override;
};
