// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

public:


	bool bIsTimerEnabled = false;
	float TimeToExplose = 0.0f;

protected:
	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime);


	virtual void ImpactProjectile() override;

	UFUNCTION()
		void TimerExplose(float DeltaTime);

	UFUNCTION(Server, Reliable)
		void Explose_OnServer();


	virtual void HitResult_OnServer(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void InitProjectile(FProjectileInfo InitParam) override;

	UFUNCTION(NetMulticast, Reliable)
	void SpawnExploseEffects_Multicast(UParticleSystem* FX, USoundBase* Sound);
	UFUNCTION(NetMulticast, Reliable)
	void SetSimulatePhysics_Multicast(bool bSimulate);
};
