// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSHealthComponent.h"
#include "TPS_IGameActor.h"

// Sets default values for this component's properties
UTPSHealthComponent::UTPSHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;


	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UTPSHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTPSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTPSHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTPSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTPSHealthComponent::ChangeCurrentHealth(float ChangeValue)
{
	if(IsCanChangeHealth(ChangeValue))
	{
		if (ChangeValue < 0)
		{
			ChangeValue = ChangeValue * CoefDamage;
		}
		if(Health > 0)
		{
			Health += ChangeValue;
			HelthChangeEvent_Multicast(Health, ChangeValue);
			//OnHealthChange.Broadcast(Health, ChangeValue);
			if (Health > 100)
			{
				Health = 100;
			}
			else
			{
				if (Health <= 0)
				{
					Health = 0;
					//DeadEvent_Multicast();
					OnDead.Broadcast();
				}
			}
		}

	}
}

bool UTPSHealthComponent::IsCanChangeHealth(float Damage)
{
	bool bIsCanChangeHealth = true;
	if (GetOwner())
	{
		ITPS_IGameActor* Interface = Cast<ITPS_IGameActor>(GetOwner());
		if (Interface)
		{
			if (Interface->IsInvulnerable())
			{
				if(Damage < 0)
				{
					bIsCanChangeHealth = false;
				}
			}
		}
	}
	return bIsCanChangeHealth;
}

void UTPSHealthComponent::HelthChangeEvent_Multicast_Implementation(float NewHealth, float ChangeValue)
{
	OnHealthChange.Broadcast(NewHealth, ChangeValue);
}

void UTPSHealthComponent::DeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}
