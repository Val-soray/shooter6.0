// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPS_IGameActor.h"
#include "TPS_EnvironmentStructure.generated.h"

UCLASS()
class SHOOTER_API ATPS_EnvironmentStructure : public AActor, public ITPS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPS_EnvironmentStructure();

	//Enums
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<UTPS_StateEffect*> Effects;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//Interface	
	UFUNCTION(BlueprintCallable)
		EPhysicalSurface GetSurfaceType() override;
		//Effects
			TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
			void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
			void AddEffect(UTPS_StateEffect* NewEffect) override;
};
