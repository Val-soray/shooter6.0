// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSPlayerController.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TPSCharacter.h"



void ATPSPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ATPSPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATPSPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}
