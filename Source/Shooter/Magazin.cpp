// Fill out your copyright notice in the Description page of Project Settings.


#include "Magazin.h"

// Sets default values
AMagazin::AMagazin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MagazinMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MagazinMesh"));
	MagazinMesh->SetupAttachment(RootComponent);

	MagazinProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("SleeveProjectileMovement"));
	MagazinProjectileMovement->UpdatedComponent = RootComponent;
	MagazinProjectileMovement->InitialSpeed;
	MagazinProjectileMovement->MaxSpeed;
	MagazinProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AMagazin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMagazin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMagazin::InitMagazin(FWeaponInfo MagazinSettings)
{
	if (MagazinMesh)
	{
		MagazinMesh->SetStaticMesh(MagazinSettings.MagazinDropMesh);
	}
	this->SetLifeSpan(MagazinSettings.MagazinLifeTime);
}
