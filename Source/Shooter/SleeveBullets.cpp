// Fill out your copyright notice in the Description page of Project Settings.


#include "SleeveBullets.h"

// Sets default values
ASleeveBullets::ASleeveBullets()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SleeveMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SleeveMesh"));
	SleeveMesh->SetupAttachment(RootComponent);

	SleeveProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("SleeveProjectileMovement"));
	SleeveProjectileMovement->UpdatedComponent = RootComponent;
	SleeveProjectileMovement->InitialSpeed = 100.f;
	SleeveProjectileMovement->MaxSpeed = 100.f;
	SleeveProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void ASleeveBullets::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASleeveBullets::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASleeveBullets::InitSleeve(FWeaponInfo SleeveSettings)
{
	if(SleeveMesh)
	{
		SleeveMesh->SetStaticMesh(SleeveSettings.SleeveBulletsMesh);
	}
	this->SetLifeSpan(SleeveSettings.SleeveLifeTime);
}
