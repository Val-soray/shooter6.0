// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Math/Color.h"
#include "WeaponDefault.h"
#include "TPS_IGameActor.h"
#include "Components/PrimitiveComponent.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TimerExplose(DeltaTime);

}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//OnServer
	bIsTimerEnabled = true;
	SetSimulatePhysics_Multicast(true);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (bIsTimerEnabled)
	{
		//OnServer
		if (TimeToExplose >= ProjectileInfo.TimeToExplose)
		{
			Explose_OnServer();
		}
		else
		{
			TimeToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::Explose_OnServer_Implementation()
{
	//OnServer
	TimeToExplose = 0.0f;

	TArray<AActor*> IgnoreActor;

	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileInfo.ExploseMaxDamage, ProjectileInfo.ExploseMaxDamage * 0.5f,
		GetActorLocation(),
		ProjectileInfo.ExploseRadius * 0.3, ProjectileInfo.ExploseRadius * 0.7,
		1, NULL, IgnoreActor, this, nullptr);

	SpawnExploseEffects_Multicast(ProjectileInfo.ExploseFX, ProjectileInfo.ExploseSound);

	DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileInfo.ExploseRadius * 0.3, 30, FColor::Red, false, 1);
	DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileInfo.ExploseRadius * 0.7, 70, FColor::Yellow, false, 1);	
	this->Destroy();
}

void AProjectileDefault_Grenade::HitResult_OnServer(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::HitResult_OnServer(HitComponent, OtherActor, OtherComponent, NormalImpulse, Hit);

}

void AProjectileDefault_Grenade::InitProjectile(FProjectileInfo InitParam)
{
	Super::InitProjectile(InitParam);

}

void AProjectileDefault_Grenade::SetSimulatePhysics_Multicast_Implementation(bool bSimulate)
{
	if(BulletMesh && BulletProjectileMovement)
	{
		BulletMesh->SetSimulatePhysics(true);
		BulletProjectileMovement->ProjectileGravityScale = 3.0f;
	}
}

void AProjectileDefault_Grenade::SpawnExploseEffects_Multicast_Implementation(UParticleSystem* FX, USoundBase* Sound)
{
	if (FX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, FTransform(FRotator(0), GetActorLocation(), FVector(1)));
	}
	if (Sound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
	}

}
