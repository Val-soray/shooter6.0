// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "WeaponDefault.h"
#include "TPS_IGameActor.h"
#include "Types.h"
#include "TPS_StateEffect.h"
#include "Perception/AISense_Damage.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
	SetReplicates(true);


	PrimaryActorTick.bCanEverTick = true;


	BulletSphere = CreateDefaultSubobject<USphereComponent>(TEXT("BulletSphere"));
	BulletSphere->SetSphereRadius(16.f);
	BulletSphere->bReturnMaterialOnMove = true;
	BulletSphere->SetCanEverAffectNavigation(false);
	RootComponent = BulletSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BulletMesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);
	BulletMesh->SetRelativeRotation(FRotator(0, 0, -90));

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("BulletProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 100.f;
	BulletProjectileMovement->MaxSpeed = 0.f;
	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
	
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	BulletSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::HitResult_OnServer);
	BulletSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BeginOverlapResult);
	BulletSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::EndOverlapResult);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ChangeProjectileDamage();
}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	//OnServer
	ProjectileInfo = InitParam;

	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileMaxSpeed;

	SetProjectileMovementSpeed_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);

	SetLifeSpan(InitParam.ProjectileLifeTime);

	ProjectileInfo = InitParam;
	
	ProjectileDamage = InitParam.ProjectileDamage;

	if(BulletMesh)
	{
		if(InitParam.BulletsMesh)
		{
			InitVisualProjectileMesh_Multicast(InitParam.BulletsMesh);
		}
		else
		{
			BulletMesh->DestroyComponent();
		}
		if(InitParam.BulletFX)
		{
			InitVisualProjectileTrail_Multicast(InitParam.BulletFX);
		}
	}
	

	LastProjectileLocation = GetActorLocation();
}

void AProjectileDefault::HitResult_OnServer_Implementation
(UPrimitiveComponent* HitComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileInfo.HitDecals.Contains(SurfaceType))
		{
			UMaterialInterface* Material = ProjectileInfo.HitDecals[SurfaceType];

			if (Material && OtherComponent)
			{
				SpawnHitDecal_Multicast(Material, OtherComponent, Hit);
				//UGameplayStatics::SpawnDecalAttached(Material, FVector(10), OtherComponent, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.f);
			}
		}
		if (ProjectileInfo.HitFXs.Contains(SurfaceType))
		{
			UParticleSystem* FX = ProjectileInfo.HitFXs[SurfaceType];

			if (FX)
			{
				SpawnHitFX_Multicast(FX, Hit);
				//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FX, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(0.5)));
			}
		}
		if (ProjectileInfo.HitSounds.Contains(SurfaceType))
		{
			USoundBase* Sound = ProjectileInfo.HitSounds[SurfaceType];
			if (Sound)
			{
				SpawnHitSound_Multicast(Sound, Hit);
				//UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Hit.ImpactPoint);
			}
		}
		
		UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, SurfaceType, Hit.BoneName);
	}
	UGameplayStatics::ApplyDamage(OtherActor, ProjectileDamage, GetInstigatorController(), this, NULL);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileDamage, Hit.Location, Hit.Location);

	ImpactProjectile();
}

void AProjectileDefault::BeginOverlapResult
(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, 
	bool bFromSweep,
	const FHitResult& SweepResult)
{

}

void AProjectileDefault::EndOverlapResult
(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{

}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}

void AProjectileDefault::ChangeProjectileDamage()
{
	FVector CurrentProjectileLocation = GetActorLocation();

	FVector LocationChange = CurrentProjectileLocation - LastProjectileLocation;

	float AllDistance = (ProjectileInfo.ProjectileLifeTime * ProjectileInfo.ProjectileInitSpeed);

	float PercentToChange = LocationChange.Size() / AllDistance;

	float RemainingDistance = GetLifeSpan()* ProjectileInfo.ProjectileInitSpeed;

	if(RemainingDistance < AllDistance * 0.9)
	{
		if(ProjectileDamage >= ProjectileInfo.ProjectileDamage * 0.5)
		{
			ProjectileDamage -= ProjectileInfo.ProjectileDamage * PercentToChange;
		}
	}

	LastProjectileLocation = GetActorLocation();
}

void AProjectileDefault::SetProjectileMovementSpeed_Multicast_Implementation(float NewSpeed, float NewMaxSpeed)
{
	if(BulletProjectileMovement)
	{
		BulletProjectileMovement->InitialSpeed = NewSpeed;
		BulletProjectileMovement->MaxSpeed = NewSpeed;
	}
}

void AProjectileDefault::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(10), OtherComp, NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.f);
}

void AProjectileDefault::SpawnHitFX_Multicast_Implementation(UParticleSystem* FXTemplate, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXTemplate, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(0.5)));
}

void AProjectileDefault::SpawnHitSound_Multicast_Implementation(USoundBase* SoundBase, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundBase, HitResult.ImpactPoint);
}

void AProjectileDefault::InitVisualProjectileMesh_Multicast_Implementation(UStaticMesh* NewMesh)
{
	if(NewMesh)
	{
		BulletMesh->SetStaticMesh(NewMesh);
	}
}

void AProjectileDefault::InitVisualProjectileTrail_Multicast_Implementation(UParticleSystem* NewTrail)
{
	UGameplayStatics::SpawnEmitterAttached(NewTrail, BulletMesh, FName("None"), FVector(0), FRotator(0, -90, 0), EAttachLocation::SnapToTarget, true);
}