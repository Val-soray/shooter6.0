// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSGameInstance.h"

bool UTPSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;
	if(WeaponInfoTaple)
	{
		WeaponInfoRow = WeaponInfoTaple->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}	
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameMode::GetInfoByName - Table have not found - NULL"));
	}
	return bIsFind;
}

bool UTPSGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTaple)
	{
		FDropItem* DropItemInfoRow;
		TArray<FName> RowNames = DropItemInfoTaple->GetRowNames();
		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropItemInfoRow = DropItemInfoTaple->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = *DropItemInfoRow;
				bIsFind = true;
			}
			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSGameMode::GetDropItemInfoByName - DropItemInfoTaple - NULL"));
	}
	return bIsFind;
}

bool UTPSGameInstance::GetEquipmentInfoByName(FName EquipName, FEquipmentInfo& OutInfo)
{
	bool bIsFind = false;
	if (EquipmentInfoTaple)
	{
		FEquipmentInfo* EquipInfoRow;

		EquipInfoRow = EquipmentInfoTaple->FindRow<FEquipmentInfo>(EquipName, "");
		if (EquipInfoRow)
		{
			bIsFind = true;
			OutInfo = *EquipInfoRow;
		}
	}
	return bIsFind;
}

void UTPSGameInstance::AddMenuWeaponToInventory(FName WeaponName, int32 Round, FString PlayerName)
{
	FInventory Inventory;
	if (InventoryForPlayers.Find(PlayerName))
	{
		Inventory = *InventoryForPlayers.Find(PlayerName);
		FWeaponSlot WeaponSlot;
		WeaponSlot.NameItem = WeaponName;
		WeaponSlot.AdditionalInfo.Round = Round;
		int8 i = 0;
		bool bIsFind = false;
		while (i < Inventory.WeaponSlots.Num() && !bIsFind)
		{
			if (Inventory.WeaponSlots.IsValidIndex(i) && Inventory.WeaponSlots[i].NameItem.IsNone())
			{
				bIsFind = true;
				Inventory.WeaponSlots[i] = WeaponSlot;
				InventoryForPlayers.Add(PlayerName, Inventory);
			}
			i++;
		}
	}
}

void UTPSGameInstance::RemoveMenuWeaponFromInventory(FName WeaponName, FString PlayerName)
{
	FInventory Inventory;
	if (InventoryForPlayers.Find(PlayerName))
	{
		Inventory = *InventoryForPlayers.Find(PlayerName);
		int8 i = 0;
		bool bIsFind = false;
		while (i < Inventory.WeaponSlots.Num() && !bIsFind)
		{
			if (Inventory.WeaponSlots[i].NameItem == WeaponName)
			{
				int32 SumWeaponSlots = 0;
				for (int j = 0; j < Inventory.WeaponSlots.Num(); j++)
				{
					if (!Inventory.WeaponSlots[j].NameItem.IsNone())
					{
						SumWeaponSlots++;
					}
				}
				int j = i;
				while (!bIsFind && j < SumWeaponSlots)
				{
					if (Inventory.WeaponSlots.IsValidIndex(j + 1))
					{
						Inventory.WeaponSlots[j] = Inventory.WeaponSlots[j + 1];
					}
					else
					{
						FWeaponSlot EmptySlot;
						Inventory.WeaponSlots[j] = EmptySlot;
						bIsFind = true;
					}
					j++;
				}
				InventoryForPlayers.Add(PlayerName, Inventory);
			}
			i++;
		}
	}
}

void UTPSGameInstance::UpdateInventoryForPlayer(FString PlayerName, int32 SumWeaponSlots, int32 SumAmmoSlots)
{
	FInventory Inventory;
	for (int32 i = 0; i < SumWeaponSlots; i++)
	{
		FWeaponSlot WeaponSlot;
		Inventory.WeaponSlots.Add(WeaponSlot);
	}
	for (int32 i = 0; i < SumAmmoSlots; i++)
	{
		FAmmoSlot AmmoSlot;
		Inventory.AmmoSlots.Add(AmmoSlot);
	}
	InventoryForPlayers.Add(PlayerName, Inventory);
}

FInventory UTPSGameInstance::GetInventoryForPlayer(FString PlayerName)
{
	FInventory Inventory;
	if(InventoryForPlayers.Contains(PlayerName))
	{
		Inventory = *InventoryForPlayers.Find(PlayerName);
	}
	return Inventory;
}

void UTPSGameInstance::AddNewInventoryForPlayer_OnServer_Implementation(const FInventory &NewInventory, const FString &PlayerName)
{	
	InventoryForPlayers.Add(PlayerName, NewInventory);
}

void UTPSGameInstance::AddNewInventoryForPlayer_Multicast_Implementation(const FInventory& NewInventory, const FString& PlayerName)
{	
	InventoryForPlayers.Add(PlayerName, NewInventory);
}
