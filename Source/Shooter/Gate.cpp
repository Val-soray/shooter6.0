// Fill out your copyright notice in the Description page of Project Settings.


#include "Gate.h"
#include "TPSCharacter.h"
#include "Tank.h"

// Sets default values
AGate::AGate()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	CollisionMesh1 = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Mesh1"));
	CollisionMesh2 = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Mesh2"));

	CollisionMesh1->SetupAttachment(RootComponent);
	CollisionMesh2->SetupAttachment(RootComponent);

	CollisionMesh1->OnComponentBeginOverlap.AddDynamic(this, &AGate::Box1OverlapReaction);
	CollisionMesh2->OnComponentBeginOverlap.AddDynamic(this, &AGate::Box2OverlapReaction);
}

// Called when the game starts or when spawned
void AGate::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGate::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	GateMove(DeltaTime);
}

void AGate::GateMove(float DeltaTime)
{
	FHitResult HitResult;
	FVector MoveDirection = FVector(0, 0, 0);
	float MoveSpeed = 250;
	FVector GateLocation = GetActorLocation();

	bool bCanGateMove = GetActorLocation().Z <= 460 && GetActorLocation().Z >= 100;


	if(bCanSetGateLocation)
	{
		if (MoveName == FName("Open"))
		{
			MoveDirection.Z = 1;
			GateLocation.Z = 100;
			SetActorLocation(FVector(GateLocation));
			bCanSetGateLocation = false;
		}
		if (MoveName == FName("Close"))
		{
			MoveDirection.Z = -1;
			GateLocation.Z = 460;
			SetActorLocation(FVector(GateLocation));
			bCanSetGateLocation = false;
		}
	}
		
	
	if(bCanGateMove)
	{
		AddActorWorldOffset(MoveDirection * MoveSpeed * DeltaTime, false, &HitResult, ETeleportType::None);
	}
}

void AGate::Box1OverlapReaction(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (OtherActor == Character->Tank)
	{
		MoveName = FName("Open");
		bCanSetGateLocation = true;
	}
}

void AGate::Box2OverlapReaction(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if (OtherActor == Character->Tank)
	{
		MoveName = FName("Close");
		bCanSetGateLocation = true;
	}
}
